  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/

#ifndef __TRACKMANAGERH__
#define __TRACKMANAGERH__

#include <OGRE/Ogre.h>
#include <Track.h>

// Clase encargada de gestionar recursos del tipo "Track".
// Funcionalidad heredada de Ogre::ResourceManager
// y Ogre::Singleton.
class TrackManager: public Ogre::ResourceManager,
                    public Ogre::Singleton<TrackManager> {
 public:
  virtual ~TrackManager();
  virtual TrackPtr load (const Ogre::String& name,
			 const Ogre::String& group = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
  static TrackManager& getSingleton ();
  static TrackManager* getSingletonPtr ();
  
 protected:
  Ogre::Resource* createImpl (const Ogre::String& name,
			      Ogre::ResourceHandle handle,
			      const Ogre::String& group,
			      bool isManual,
			      Ogre::ManualResourceLoader* loader,
			      const Ogre::NameValuePairList* createParams);
   TrackManager();

};

#endif
