  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef CUSTOM_OBJECT_H
#define CUSTOM_OBJECT_H
#include "AnimationBlender.h"
#include <OIS/OIS.h>
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"

using namespace Ogre;

enum OBJECT_STATE {
	LIVE, DEAD, END
};

class InteractiveObject{
protected:
  SceneNode *_mMainNode; // Main character node
  Entity *_mEntity; // Character entity
  SceneManager *_mSceneMgr;
  AnimationBlender *_mAnimation;
  
  //dynamics
  OgreBulletDynamics::RigidBody* _rigidBody;
  OgreBulletDynamics::DynamicsWorld* _world;
  OgreBulletCollisions::CollisionShape* _sceneBoxShape;
  std::string _mName;
  Ogre::Vector3 _position;
  bool _visible;
  OBJECT_STATE _stateObject;
  AnimationState *_currentAnimation;

public:
  InteractiveObject();
  InteractiveObject(Ogre::SceneManager* sceneMgr,
                OgreBulletDynamics::DynamicsWorld* world,
                const String& name, const String& mesh,
                const Ogre::Vector3& v_pos);
  virtual ~InteractiveObject();
  
  SceneNode *getMainNode() const;
  Ogre::Entity* getMainEntity() const;
  SceneManager * getSceneManager() const;
  AnimationBlender * getAnimation() const;
  OgreBulletDynamics::RigidBody * getRigidBody() const;
  OgreBulletCollisions::CollisionShape * getCollisionShape() const;
  Ogre::Vector3  getPosition() const;
  OgreBulletDynamics::DynamicsWorld * getWorld() const;
  Ogre::Vector3 getWorldPosition() const;
  bool isVisible() const;
  std::string getName() const;
  
  void turn_angle ( const Ogre::Radian& angle );
  void turn_left (  );
  void turn_right (  );
  virtual  void update (Real elapsedTime, const  OIS::Keyboard* input, const OIS::Mouse* mouseInput) = 0;
//   virtual void liberate() = 0;
//   virtual update(double timeSinceLastFrame);
  
};
#endif