  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
  
#ifndef CUSTOM_CHARACTER_H
#define CUSTOM_CHARACTER_H
#include <Ogre.h>
// #include "AnimationBlender.h"
#include <OIS/OIS.h>
#include "InteractiveObject.h"
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#define VELOCIDAD 2.0
#define VELOCIDAD_ANIMACION 1.0
#define VELOCITY_SHOT 50.0f

#define HEALTH_SHOT 25.0

#define VELOCIDAD_RUNNING 5.0
//#define VELOCIDAD_RUNNING_ANIMACION 6.0

#define MAX_HEALTH 100.0
#define MAX_MAGIC 100.0

#define ZERO_DISTANCE 0.0

// Animation Name of Mesh
// #define MOVE_ANIMATION "Move"
// #define STOP_ANIMATION "Stop"

/// \brief timer show the hostage when is death
#define TIMER_PATICLE_DEATH 2.0f

// Position of shot with respect to character
// #define POSITION_SHOT Ogre::Vector3(-0.2780,1.0726,1.6939)
// #include "physics.h"

using namespace Ogre;

// Generic Character class
enum class Status{Attack, Idle, Alert};

 class Character : public InteractiveObject{
     protected:

	 Status _status;


	 //Dummy objects
	 Ogre::Entity* _entityDummy;

	 Ogre::SceneNode*  _nodeDummy;
	 Ogre::ParticleSystem* _magic;
 	 int _life;
	 int _level;


     public:
         // Updates the character (movement...)
       Character();
        Character ( Ogre::SceneManager* sceneMgr,
                OgreBulletDynamics::DynamicsWorld* world,
                const String& name, const String& mesh,
                const Ogre::Vector3& v_pos);
	/// \brief default destructor
	virtual ~Character();
	/// \brief copy constructor
	/// \param other source character to copy
	Character ( const Character& other );
	/// \brief assignment operator to copy another character
	/// \param other source character to copy
	Character&    operator= ( const Character& other );
  
	void copy ( const Character& source );

         virtual  void update (Real elapsedTime, const  OIS::Keyboard* input, const OIS::Mouse* mouseInput) = 0;
         // The three methods below returns the two camera-related nodes, 
         // and the current position of the character (for the 1st person camera)
	 SceneNode *getDummyNode() const;

	 Ogre::Entity* getDummyEntity() const;
	 Status getStatus() const;
	
	 Ogre::ParticleSystem* getMagic() const;
	 bool isDeath() const;
	 void setHealth(int life);
	 int getHealth() const;
	 int getLevel() const;
// 	 void updateLifeBar();
//          virtual void createLifeBar()=0;
	 virtual void showDummy(bool show)=0;
	 virtual void death()=0;
	 
 };
 
#endif