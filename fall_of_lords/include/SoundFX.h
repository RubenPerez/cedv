  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/

#ifndef __SOUNDFXH__
#define __SOUNDFXH__

#include <SDL/SDL_mixer.h>
#include <OGRE/Ogre.h>
#include <OGRE/OgreResourceManager.h>

// Clase para mezclar efectos de sonido.
// Delega en SDL mixer.
class SoundFX: public Ogre::Resource {
 public:
  // Constructor (ver Ogre::Resource).
  SoundFX(Ogre::ResourceManager* creator,
	  const Ogre::String& resource_name,
	  Ogre::ResourceHandle handle,
	  const Ogre::String& resource_group,
	  bool isManual = false,
	  Ogre::ManualResourceLoader* loader = 0);
  
  ~SoundFX();

  int play(int loop = 0);
        
 protected:
  void loadImpl();
  void unloadImpl();
  size_t calculateSize() const;
  
 private:
  Mix_Chunk* _pSound; // Info sobre el efecto de sonido.
  Ogre::String _path; // Ruta completa al efecto de sonido.
  size_t _size;       // Tamaño del efecto (bytes).
};


class SoundFXPtr: public Ogre::SharedPtr<SoundFX> {
 public:
  // Es necesario implementar constructores y operador de asignación.
  SoundFXPtr(): Ogre::SharedPtr<SoundFX>() {}
  explicit SoundFXPtr(SoundFX* s): Ogre::SharedPtr<SoundFX>(s) {}
  SoundFXPtr(const SoundFXPtr& s): Ogre::SharedPtr<SoundFX>(s) {}
  SoundFXPtr(const Ogre::ResourcePtr& r);

  SoundFXPtr& operator= (const Ogre::ResourcePtr& r);
};

#endif
