  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef MYTEXTURELISTENER_H
#define MYTEXTURELISTENER_H

#include <Ogre.h>

using namespace std;
using namespace Ogre;

/// \brief class to be used how listener in the texture for the conversion to black & white colours
/// This class performs a setMaterialName() in the preRender to black/white colours for the object
/// int the texture, also in the postRender reset Material to origin values. In the class, there is
/// a method which checks white/black colour levels to tell us if any object exists in the scene
class MyTextureListener : public RenderTargetListener
{
  private:
    /// \brief reference to scene manager
    Ogre::SceneManager* _sceneMgr;
    /// \brief texture pointer
    Ogre::TexturePtr _rtt;
    /// \brief boolean value to check if there is any object in the texture
    bool _enemyViewed;
    /// \brief material name will be get in pre render operation and reset in post render operation
    string _materialName;

  public:
    /// \brief constructor for the texture listener
    /// \param sceneMgr reference to scene manager
    /// \param rtt texture pointer
    MyTextureListener ( Ogre::SceneManager* sceneMgr, const Ogre::TexturePtr& rtt );
    /// \brief default destructor
    ~MyTextureListener();
    /// \brief this method is executed before the texture is put into the rectangle2D
    /// \param evt event
    virtual void preRenderTargetUpdate ( const RenderTargetEvent& evt );
    /// \brief this method is executed after the texture is put into the rectangle2D
    /// \param evt event
    virtual void postRenderTargetUpdate ( const RenderTargetEvent& evt );
    /// \brief this method returned if there is any object in the texture
    /// \return true/false according to any object is in the texture
    inline bool enemyViewed() { return _enemyViewed; };
};

#endif // MYTEXTURELISTENER_H
