   /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef ITEM_H
#define ITEM_H
#include <Ogre.h>
#include "Character.h"
#include "SoundFXManager.h"

/// \brief timer show the hostage when is liberate
#define TIMER_PATICLE_LIBERATE 2.0


class Item : public InteractiveObject
{
  public:
    /// \brief Hostage constructor parametrized
    /// \param sceneMgr reference to scene manager (ogre)
    /// \param world reference to dynamic world (bullet)
    /// \param name name to identify the Hostage
    /// \param initia_pos initial position in coordenate (X, Y, Z)
    Item     ( Ogre::SceneManager* sceneMgr,
            OgreBulletDynamics::DynamicsWorld* world,
            const std::string& name, const std::string& mesh,
            const Ogre::Vector3& v_pos,
            Character* ptrHero , const std::string& music );
    virtual ~Item();
   
    void update (Real elapsedTime);
    virtual void effect()=0;
    void update (Real elapsedTime, const  OIS::Keyboard* input, const OIS::Mouse* mouseInput){}

    double _timerParticleLiberate;
     Character * _refHero;
    SoundFXPtr _soundFreeFX;

  protected:



  private:
    /// \brief reference to the particle system used to hostage liberation
    Ogre::ParticleSystem* _particleLiberation;
    /// \brief reference to scene node for the particle system
    Ogre::SceneNode* _particleLiberationNode;
    /// \brief hostage state
//     STATE_HOSTAGE _state;
    /// \brief sound used when the hostage was released
};


#endif 

