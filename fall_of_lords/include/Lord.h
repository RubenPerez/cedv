  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
  
#ifndef LORD_H
#define LORD_H

#include "Enemy.h"
#include "SoundFXManager.h"

// #include "Character.h"


class Lord: public Enemy {
protected:
      SoundFXPtr _soundAttack;

  
  
public: 
   Lord ( Ogre::SceneManager* sceneMgr,
            OgreBulletDynamics::DynamicsWorld* world,
            const std::string& name, const std::string& mesh,
            const Ogre::Vector3& v_pos,
            Character* ptrHero );
   virtual ~Lord();
  void update(float timeSinceLastFrame, OIS::Keyboard const*, OIS::Mouse const*);
  void attack(float timeSinceLastFrame);
  void death();
  void changeAnimation(const std::string & nameAnimation);
  void createLifeBar();

};


#endif