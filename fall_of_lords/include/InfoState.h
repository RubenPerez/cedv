  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/

#ifndef InfoState_H
#define InfoState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <string>
#include <sstream>
#include <fstream>
#include <CEGUI.h>
#include "GameState.h"
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

class InfoState : public Ogre::Singleton<InfoState>, public GameState
{
 public:
  InfoState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static InfoState& getSingleton ();
  static InfoState* getSingletonPtr ();
  
  std::string convert(std::string name, double time);
  

  bool start1(const CEGUI::EventArgs &e);
  bool back(const CEGUI::EventArgs &e);

  //GUI
  void createBackground();
  void createGUI();

 protected:
  Ogre::Rectangle2D* _rect;
  CEGUI::OgreRenderer* renderer; 
  CEGUI::Window* _sheet,* _about,* _controls;

};

#endif
