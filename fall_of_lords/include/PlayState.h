  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OISKeyboard.h>
#include "GameState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "InputManager.h"
#include <sstream>
#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <fstream>
#include <memory>
#include "AnimationBlender.h"
#include "OgreCharacter.h"
#include "Enemy.h"
#include "Potion.h"
#include "Key.h"
#include "Item.h"
#include "Character.h"
#include "MiniMapTextureListener.h"
#include "ExtendedCamera.h"
#include "Fader.h"
#include "Lord.h"
#include "WinState.h"
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"	
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include <list>
#include <vector>
#include <typeinfo>
#define HIGHT_CAMERA 35.0f
#define HIGHT_CAMERA_MINIMAP 50.0f
#define DISTANCE_Z_CAMERA 5.0f
#define DISTANCE_Z_CAMERA_MINIMAP 1.0f


using namespace Ogre;
class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}
  
  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();
  void setDifficult(int);

 protected:
  Ogre::Camera *_mmCamera;
  Ogre::SceneNode *_mmNode;
//   Ogre::SceneNode* _ground;
  Ogre::Light* _light;
  Ogre::Real deltaT;
//   Ogre::Rectangle2D* _rect;
//   Ogre::MaterialPtr _material;
//   AnimationBlender *_animBlender;
//   TrackPtr _mainTrack;
  OgreCharacter * mChar;
  Lord* mLord;
  ExtendedCamera * mExCamera;
//   Ogre::Viewport * _viewport;
  OgreBulletDynamics::DynamicsWorld * _world;
  Ogre::StaticGeometry * _staticGeometry;
  OgreBulletDynamics::RigidBody * _rigidTrack;
  std::list<Character*> _characters;
  OgreBulletCollisions::TriangleMeshCollisionShape* _trackTrimesh;
MiniMapTextureListener*   _textureListener;
    /// \brief reference to render to texture used in the minimap
    Ogre::RenderTexture*      _rtex;
    
    std::list<Enemy*> _enemies;
     std::vector<Fader*>       _vFader;
   std::list<Item*> _items;
      int _numEntities;
  float _timeLastObject;

//   std::deque <OgreBulletDynamics::RigidBody *>         _bodies;
//   std::deque <OgreBulletCollisions::CollisionShape *>  _shapes;
 private:
  OIS::MouseState _mouse_position;
  bool _key_pressed;
  OIS::KeyCode _event;
 
  Ogre::Real _time_count,_last_time;
  Ogre::OverlayManager* _overlayManager;
  Ogre::ParticleSystem* _particleLiberation;
  Ogre::SceneNode* _node;
    Ogre::SceneNode* _particleLiberationNode;
  TrackPtr _mainTrack;
  std::vector<Ogre::Vector3> initEnemiesCoord();
  void initPhysics();
  void deletePhysics();
  void createInitialWorld();
  void createMiniMap();
  void updateMiniMap(double);
  void updatePanelLife();
  void deleteMiniMap();
  void putOverlay ( std::string name, bool visible );
  void reset();
  void collocateEnemies();
   void collocatePotions();
  void doorOpen();
  void doorClose();
  Enemy* detectCollisionHeroWithEnemies(OgreCharacter* hero, std::list<Enemy*> enemies);
  Item* detectCollisionHeroWithItems(OgreCharacter* hero, std::list<Item*> );

};

#endif

