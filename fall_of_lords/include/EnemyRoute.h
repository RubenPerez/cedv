
  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
  #ifndef ENEMYROUTE_H
#define ENEMYROUTE_H

#include <OgreVector3.h>
#include <random>
/// \brief Class to manage the enemy route
class EnemyRoute
{
  public:
    /// \brief Default constructor
    EnemyRoute();
    EnemyRoute(Ogre::Vector3 center, float radius);
    /// \brief Default destructor
    virtual ~EnemyRoute();
   Ogre::Vector3 getNextPoint(Ogre::Vector3 source) ;
   Ogre::Vector3 getCenter() const;
   float getRadius() const;
   Ogre::Vector3 getCurrentPoint() const;//point to achieve

  protected:
    Ogre::Vector3 _center, _currentPoint;
    float _radius;

private:
  float rand_FloatRange(Ogre::Real);
  
};

#endif // ENEMYROUTE_H