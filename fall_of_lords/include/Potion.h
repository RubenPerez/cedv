   /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef POTION_H
#define POTION_H
#include <Ogre.h>
#include "Character.h"
#include "SoundFXManager.h"
#include "Item.h"


class Potion : public Item
{
  public:
    /// \brief Hostage constructor parametrized
    /// \param sceneMgr reference to scene manager (ogre)
    /// \param world reference to dynamic world (bullet)
    /// \param name name to identify the Hostage
    /// \param initia_pos initial position in coordenate (X, Y, Z)
    Potion     ( Ogre::SceneManager* sceneMgr, 
            OgreBulletDynamics::DynamicsWorld* world,
            const std::string& name, const std::string& mesh,
            const Ogre::Vector3& v_pos,
            Character* ptrHero, const std::string& music, int life  );
    virtual ~Potion();
   
   
    void effect();
    
private:
  int _recoverLife;
  
};


#endif 
