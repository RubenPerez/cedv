  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/

#ifndef __SOUNDFXMANAGERH__
#define __SOUNDFXMANAGERH__

#include <OGRE/Ogre.h>
#include "SoundFX.h"

// Clase encargada de gestionar recursos del tipo "SoundFX".
// Funcionalidad heredada de Ogre::ResourceManager
// y Ogre::Singleton.
class SoundFXManager: public Ogre::ResourceManager,
                      public Ogre::Singleton<SoundFXManager> {
 public:
  virtual ~SoundFXManager();

  virtual SoundFXPtr load(const Ogre::String& name,
			  const Ogre::String& group = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

  virtual SoundFXPtr getMusic(const Ogre::String& name);

  static SoundFXManager& getSingleton();
  static SoundFXManager* getSingletonPtr();

  static int getAvailableChannels();
  
 protected:
  Ogre::Resource* createImpl(const Ogre::String& name,
			     Ogre::ResourceHandle handle,
			     const Ogre::String& group,
			     bool isManual,
			     Ogre::ManualResourceLoader* loader,
			     const Ogre::NameValuePairList* createParams);
    
 private:
  static int _numChannels;
  SoundFXManager();

};

#endif
