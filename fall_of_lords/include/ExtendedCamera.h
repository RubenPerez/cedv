  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
  
#ifndef EXTENDED_CAMERA_H
#define EXTENDED_CAMERA_H
 
 #include <Ogre.h>
 
 // Our extended camera class
 class ExtendedCamera {
 // Attributes ------------------------------------------------------------------------------
     protected:
         Ogre::SceneNode *mTargetNode; // The camera target
         Ogre::SceneNode *mCameraNode; // The camera itself
         Ogre::Camera *mCamera; // Ogre camera
 
         Ogre::SceneManager *mSceneMgr;
         Ogre::String mName;
	 int mMode;
         bool mOwnCamera; // To know if the ogre camera binded has been created outside or inside of this class
 
         Ogre::Real mTightness; // Determines the movement of the camera - 1 means tight movement, while 0 means no movement
     public:
 // Methods ---------------------------------------------------------------------------------
     int getMode();
     void setMode(int mode);
     protected:
     public:
         ExtendedCamera (Ogre::String name, Ogre::SceneManager *sceneMgr, Ogre::Camera *camera);
         ~ExtendedCamera ();
 
         void setTightness (Ogre::Real tightness);
         Ogre::Real getTightness ();
 
         Ogre::Vector3 getCameraPosition ();
 
         void instantUpdate (Ogre::Vector3 cameraPosition, Ogre::Vector3 targetPosition);
	 
         void update (Ogre::Real elapsedTime, Ogre::Vector3 cameraPosition, Ogre::Vector3 targetPosition);
	 Ogre::Camera * getCamera(){return this->mCamera;}
 };
 
#endif