  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef MINIMAPTEXTURELISTENER_H
#define MINIMAPTEXTURELISTENER_H

#include <Ogre.h>
#include <list>
#include "Character.h"

using namespace std;
using namespace Ogre;

/// \brief class to be used how listener in the texture for the mini map
/// This class performs a change between real objects (enemyes, heroes, hostages) and dummy objects (cubes with colors)
class MiniMapTextureListener : public RenderTargetListener
{
  private:
    /// \brief list of chracteres in the scene
    std::list<Character*> *_vCharacteres;
    /// \brief reference to scene manager
    Ogre::SceneManager* _sceneMgr;

  public:
    /// \brief constructor for the texture listener
    /// \param sceneMgr reference to scene manager
    /// \param rtt texture pointer
    MiniMapTextureListener ( Ogre::SceneManager* sceneMgr, std::list<Character*> *vCharacteres);
    /// \brief default destructor
    ~MiniMapTextureListener();
    /// \brief this method is executed before the texture is put into the rectangle2D
    /// \param evt event
    virtual void preRenderTargetUpdate ( const RenderTargetEvent& evt );
    /// \brief this method is executed after the texture is put into the rectangle2D
    /// \param evt event
    virtual void postRenderTargetUpdate ( const RenderTargetEvent& evt );

};

#endif // MINIMAPTEXTURELISTENER_H

