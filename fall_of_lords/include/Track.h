  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/

#ifndef __TRACKH__
#define __TRACKH__

#include <SDL/SDL_mixer.h>
#include <OGRE/Ogre.h>

class Track : public Ogre::Resource {
 public:
  // Constructor (ver Ogre::Resource).
  Track (Ogre::ResourceManager* pManager,
	 const Ogre::String& resource_name,
	 Ogre::ResourceHandle handle,
	 const Ogre::String& resource_group,
	 bool manual_load = false,
	 Ogre::ManualResourceLoader* pLoader = 0);
  ~Track ();

  // Manejo básico del track.
  void play (int loop = -1);
  void pause ();
  void stop ();
  
  void fadeIn (int ms, int loop);
  void fadeOut (int ms);
  static bool isPlaying ();

 private:
  // Funcionalidad de Ogre::Resource.
  void loadImpl ();   
  void unloadImpl (); 
  size_t calculateSize () const;

  // Variables miembro.
  Mix_Music* _pTrack; // SDL
  Ogre::String _path; // Ruta al track.
  size_t _size;       // Tamaño.
};

// Smart pointer a Track.
class TrackPtr: public Ogre::SharedPtr<Track> {
 public:
  // Es necesario implementar constructores y operador de asignación.
 TrackPtr(): Ogre::SharedPtr<Track>() {}
 explicit TrackPtr(Track* m): Ogre::SharedPtr<Track>(m) {}
 TrackPtr(const TrackPtr &m): Ogre::SharedPtr<Track>(m) {}
 TrackPtr(const Ogre::ResourcePtr &r);
 TrackPtr& operator= (const Ogre::ResourcePtr& r);
};

#endif
