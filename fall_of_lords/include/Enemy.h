  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
  
#ifndef ENEMY_H
#define ENEMY_H

#include <Ogre.h>
#include "Character.h"
#include "EnemyRoute.h"
#include "MyTextureListener.h"

#define SIZE_LIFE_BAR 2.0f
#define NAME_TEXTUTE_CAM "RttT_"
#define NAME_MATERIAL_CAM "RttMat_"
#define TIMER_MAX_BLOCKED 3.0f
#define INTERVAL_ATTACK 2
#define ENEMY_DAMAGE 16
#define VISION_DISTANCE 15
#define STOP_ANIMATION "Idle"
#define WALK_ANIMATION "Walk"
#define RUN_ANIMATION "Run"
#define ATTACK_ANIMATION "Attack"

/// \brief enumerator with the states of our enemy character
enum STATES_ENEMY {
  WATCHING,
//   ALERT,
  ATTACK,
  CHASING
};

class Enemy : public Character
{
  public:
    /// \brief Enemy constructor parametrized
    /// \param sceneMgr reference to scene manager (ogre)
    /// \param world reference to dynamic world (bullet)
    /// \param name name to identify the Enemy
    /// \param v_pos initial position in coordenate X, Y, Z with object Ogre::Vector3
    /// \param route route of enemy
    /// \param ptrHero reference to hero
    Enemy ( Ogre::SceneManager* sceneMgr,
            OgreBulletDynamics::DynamicsWorld* world,
            const std::string& name, const std::string& mesh,
            const Ogre::Vector3& v_pos,
            Character* ptrHero );
    /** Default destructor */
    virtual ~Enemy();
  
     inline const Ogre::TexturePtr&  getTexturePtr() { return _rtt; };
    /// \brief method to get render to texture reference
    /// \return render to texture reference
    inline Ogre::RenderTexture*     getRenderTexture() const { return _rtex; };
    
 
    	   void          death();
	   
 	 SceneNode *getLifeNode() const;
	 Ogre::Billboard *getBillBoardNode() const;
	  Ogre::BillboardSet* getBillBoardSet() const;
	 Ogre::Billboard * getLifeBar() const;

    bool                            haveYouSeenAnybody();
  
  
    bool                            walk_to ( const Ogre::Vector3& pos,float timeSinceLastFrame, bool running = false );
    void                            showDummy ( bool show );

    void                            reorient_enemy_to_hero();
 
    void                            setCurrentState ( const STATES_ENEMY& newState );
    inline const STATES_ENEMY&     getCurrentState() { return _currentState; };

    
    	 //billboards
	 Ogre::BillboardSet* _bbSetLife;
	 Ogre::SceneNode* _lifeNode;
	 Ogre::Billboard* _lifeBar;
	 
	 
  protected:
    void setVisible(bool);
    virtual void createLifeBar();
    virtual void update (float elapsedTime, const  OIS::Keyboard* input, const OIS::Mouse* mouseInput);
     virtual void changeAnimation(const std::string& nameAnimation);
 
    double _timerParticleDeath;
    
   Ogre::TexturePtr _rtt;
    /// \brief reference to render to texture object
    Ogre::RenderTexture* _rtex;
    /// \brief reference to camera used to enemy POV
    Ogre::Camera* _camPOV;
    /// \brief reference to texture listener
    MyTextureListener* _textureListener;
    EnemyRoute _route;

   	  Ogre::ParticleSystem* _particleDeath;
	Ogre::SceneNode* _particleDeathNode;
    /// \brief current enemy state
    STATES_ENEMY _currentState;
  Ogre::Vector3 _currentPosition;

    Character* _refHero;
  
    float _lastHealth;
    float _timeLastAttack;
    float _timeLostVision;
    bool _animated;
    bool isEqualPoint (Ogre::Vector3 point1, Ogre::Vector3 point2);
    
    bool isInContact();
    
    void walk( bool reverse, float velocidad, float timeSinceLastFrame);
    void run( bool reverse, float velocidad, float timeSinceLastFrame);

    void attack(float timeSinceLastFrame);
   
    void updateLifeBar();

};

#endif // ENEMY_H
