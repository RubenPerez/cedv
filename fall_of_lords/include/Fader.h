  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef FADER_H
#define FADER_H


namespace Ogre {
   class TextureUnitState;
   class Overlay;
}

/// \brief Class to receive the callbacks from Fader class
class FaderCallback
{
public:
   /// \brief this callback was launched when the Fader is IN
   virtual void fadeInCallback(void) {}
   /// \brief this callback was launched when the Fader is OUT
   virtual void fadeOutCallback(void) {}
};

/// \brief Class to manage the Fader effect on the overlays
class Fader
{
public:
  /// \brief Fader Constructor
  /// \param OverlayName Overlay identifier to use in the Fader effect
  /// \param MaterialName Material name to use in the Fader effect
  /// \param instance FaderCallback instance to invoke in the IN/OUT Fader effect
  Fader(const char *OverlayName, const char *MaterialName, FaderCallback *instance = 0);
  /// \brief Fader destructor
   ~Fader(void);

  /// \brief Method to configure the duration for the Fader IN effect
  /// \param duration duration in seconds
  void startFadeIn(double duration = 1.0f);
  /// \brief Method to configure the duration for the Fader OUT effect
  /// \param duration duration in seconds
  void startFadeOut(double duration = 1.0f);
  /// \brief Method to execute Fader effect (in the update() on GameState class)
  /// \param timeSinceLastFrame time used in update method to know the time between frames
  void fade(double timeSinceLastFrame);

protected:
  /// \brief alpha channel
  double _alpha;
  /// \brief current duration
  double _current_dur;
  /// \brief total duration
  double _total_dur;
  /// \brief reference to callback instance
  FaderCallback *_inst;
  /// \brief reference to texture unit state
  Ogre::TextureUnitState *_tex_unit;
  /// \brief overlay reference
  Ogre::Overlay *_overlay;

  enum _fadeop {
       FADE_NONE,
       FADE_IN,
       FADE_OUT,
  } _fadeop;

};
#endif // FADER_H
