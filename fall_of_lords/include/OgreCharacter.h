  /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
#ifndef OGRE_CHARACTER_C
#define OGRE_CHARACTER_C
#include <Ogre.h>
#include "Character.h"
#include "SoundFXManager.h"

enum States {
  IDLE,
  ATTACK1,
  JUMPING,
  RUNNING,
  WALKING
};

class OgreCharacter : public Character {

 // Attributes ------------------------------------------------------------------------------
     protected:
         SceneNode *_mSightNode; // "Sight" node - The character is supposed to be looking here
         SceneNode *_mCameraNode; // Node for the chase camera
	 bool _animated;
	 Ogre::SceneNode* _magicNode;
	 Ogre::Billboard* _magicBar;
	 int _pm;
	 
	 bool _inHands;
	  States _state;
	  float _lastAttack;
	  bool _key;
	  
	 SoundFXPtr _attack1 = SoundFXManager::getSingletonPtr()->load("hit.wav");


     public:
 // Methods ---------------------------------------------------------------------------------
     protected:
    

     public:
         OgreCharacter (Ogre::String name, Ogre::String mesh, SceneManager *sceneMgr, Ogre::Vector3 posIni, 
			OgreBulletDynamics::DynamicsWorld* world, int life, int pm);
         virtual ~OgreCharacter ();
         void update (Ogre::Real elapsedTime, const  OIS::Keyboard* input, const OIS::Mouse* mouse);
	 void swapRun2Walk();
// 	    void createMagicBar();
//        void createLifeBar();
	 void turnFrontBack();
	 void showDummy(bool show);
	 void stopAnimation();
         // Change visibility - Useful for 1st person view ;)
         void setVisible (bool visible); 
	 
	 void runStart();
	  void runStop();
	  void walkStart();
	  void walkStop();
	  void jumpStart();
	  void jumpStop();
	  void atack1Start();
	  void atack1Stop();
	  void atack2Start();
	  void atack2Stop();
	  bool onGround();
	  bool isAttack();
	
	  void death();
	 
	  void setKey(bool l){ _key=l;}
	  bool getKey(){ return _key;}

	 Ogre::SceneNode*
	    getSightNode () {
             return _mSightNode;
	  }
	SceneNode *getCameraNode () {
             return _mCameraNode;
         }
       
	 
 };
 
#endif