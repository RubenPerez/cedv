#include "IntroState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

IntroState::IntroState(){
  _pTrackManager = nullptr;
}

void
IntroState::enter ()
{
  GameManager::getSingletonPtr()->getLog()->logMessage("Entering intro state");
  Ogre::Root* root = Ogre::Root::getSingletonPtr();
  _pTrackManager = TrackManager::getSingletonPtr();
  _mainTrack = _pTrackManager->load("menu_music.mp3");
  _pSoundFXManager =  SoundFXManager::getSingletonPtr();
  _simpleEffect = _pSoundFXManager->load("boton.wav");
  
    // // Reproducción del track principal...
    _mainTrack->play();

  _sceneMgr = root->createSceneManager(Ogre::ST_GENERIC, "IntroSceneManager");
  _camera = _sceneMgr->createCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
  
    Ogre::Light *light1 = _sceneMgr->createLight("Light1");
   light1->setPosition(-0.009391,30,-7.749614);
  light1->setType(Ogre::Light::LT_SPOTLIGHT);
  light1->setDirection(Ogre::Vector3(0.07,-0.008,0.998));
   light1->setSpotlightInnerAngle(Ogre::Degree(25.0f));
  light1->setSpotlightOuterAngle(Ogre::Degree(60.0f));
  light1->setSpotlightFalloff(0.0f);
  
  
//   _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);
  
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  Ogre::Overlay *overlay = overlayManager->getByName("Logo");
  overlay->show();
  
  createBackground();

  createGUI();
  
   _exitGame = false;

  // Reproducción del track principal...
//   this->_mainTrack->play();
  
}

void IntroState::createBackground(){
  
    // Create background material
  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Backgro", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("6928706-fantasy-world.jpg");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
  
  // Create background rectangle covering the whole screen
  _rect = new Ogre::Rectangle2D(true);
  _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  _rect->setMaterial("Backgro");
  
  //   Render the background before everything else
  _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
  
  // Attach background to the scene
  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Backgro");
  node->attachObject(_rect);
}


void IntroState::createGUI()
{
  
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Ex1/Sheet");
  
  _newButton = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/NewGameButton");
  _newButton->setText("Juego Nuevo");
  _newButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.27,0),CEGUI::UDim(0.08,0)));
  _newButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.37,0),CEGUI::UDim(0.5,0)));

  _newButton->subscribeEvent(CEGUI::PushButton::EventClicked,
 			     CEGUI::Event::Subscriber(&IntroState::start, 
						      this));
  _sheet->addChildWindow(_newButton);
  
  _loadButton = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/CreditsButton");
  _loadButton->setText("Creditos");
  _loadButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.27,0),CEGUI::UDim(0.08,0)));
  _loadButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.37,0),CEGUI::UDim(0.6,0)));

  _loadButton->subscribeEvent(CEGUI::PushButton::EventClicked,
 			     CEGUI::Event::Subscriber(&IntroState::credit, 
						      this));
  
  _sheet->addChildWindow(_loadButton);
  
 

  _recordsButton = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/RecordsButton");
  _recordsButton->setText("Records");
  _recordsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.27,0),CEGUI::UDim(0.08,0)));
  _recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.37,0),CEGUI::UDim(0.7,0)));
  _sheet->addChildWindow(_recordsButton);
  _recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&IntroState::record,this));
  _sheet->addChildWindow(_recordsButton);

  
  CEGUI::System::getSingleton().setGUISheet(_sheet);
  
  _quitButton = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/QuitButton");
  _quitButton->setText("Salir");
  _quitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.27,0),CEGUI::UDim(0.08,0)));
//   _quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.35,0),CEGUI::UDim(0.77,0)));
    _quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.65,0),CEGUI::UDim(0.80,0)));

  _quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&IntroState::quit, 
						      this));
  _sheet->addChildWindow(_quitButton);
  
    _rulesOptionsButton = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/RulesOptions");
  _rulesOptionsButton->setText("Reglas y Controles");
  _rulesOptionsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.27,0),CEGUI::UDim(0.08,0)));
  _rulesOptionsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05,0),CEGUI::UDim(0.80,0)));
  _rulesOptionsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
 			     CEGUI::Event::Subscriber(&IntroState::options, 
						      this));
  
  _sheet->addChildWindow(_rulesOptionsButton);
  
}

bool IntroState::start (const CEGUI::EventArgs &e){
  
  _simpleEffect->play();
  pushState(PlayState::getSingletonPtr());
  _mainTrack->pause();
  return true;
}

bool IntroState::quit (const CEGUI::EventArgs &e){
  
  _simpleEffect->play();
  _exitGame = true;
  return true;
}

bool IntroState::credit (const CEGUI::EventArgs &e){
    _simpleEffect->play();
  pushState(CreditState::getSingletonPtr());
  return true;
}

bool IntroState::record (const CEGUI::EventArgs &e){
 
  _simpleEffect->play();
  pushState(RecordState::getSingletonPtr());
  return true;
}

bool IntroState::options (const CEGUI::EventArgs &e){
  _simpleEffect->play();
   pushState(InfoState::getSingletonPtr());
  
  return true;
}


void
IntroState::exit()
{  
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/QuitButton");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/RecordsButton");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/RulesOptions");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/CreditsButton");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheet");
 
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheet");
  Ogre::MaterialManager::getSingleton().remove("Background");   
  delete _rect;
   
 
 _sceneMgr->destroyLight("light1");
 _sceneMgr->destroyCamera(_camera);   
   _sceneMgr->destroySceneNode("Backgro");

   if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);
//    delete _pTrackManager; 
//   _sceneMgr->clearScene();
  
//   _mainTrack->stop();
}

void
IntroState::pause ()
{
  
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  Ogre::Overlay *overlay = overlayManager->getByName("Logo");
  overlay->hide();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/QuitButton")->hide();
 CEGUI::WindowManager::getSingleton().getWindow("Ex1/RecordsButton")->hide();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/CreditsButton")->hide();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/Sheet")->hide();
  
   CEGUI::WindowManager::getSingleton().getWindow("Ex1/QuitButton")->deactivate();
 CEGUI::WindowManager::getSingleton().getWindow("Ex1/RecordsButton")->deactivate();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/CreditsButton")->deactivate();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/Sheet")->deactivate();
  
  
   CEGUI::WindowManager::getSingleton().getWindow("Ex1/QuitButton")->releaseInput();
 CEGUI::WindowManager::getSingleton().getWindow("Ex1/RecordsButton")->releaseInput();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/CreditsButton")->releaseInput();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/Sheet")->releaseInput();
  
  
     _mainTrack->stop();
_pTrackManager->unload("menu_music.mp3");
//   CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/QuitButton");
//   CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/RecordsButton");
//   CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/CreditsButton");
//   CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheet");
//  
//   CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheet");
//     CEGUI::WindowManager::getSingleton().destroyAllWindows();

  //TODO parar musica
//   _node->setVisible(false,true);
//    _mainTrack->stop();
}


void
IntroState::resume (){
// {  _node->setVisible(true,false);
      CEGUI::System::getSingleton().setGUISheet(_sheet);

GameManager::getSingletonPtr()->getViewport()->setCamera(_camera);
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  Ogre::Overlay *overlay = overlayManager->getByName("Logo");
  overlay->show();

    CEGUI::WindowManager::getSingleton().getWindow("Ex1/QuitButton")->show();
 CEGUI::WindowManager::getSingleton().getWindow("Ex1/RecordsButton")->show();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/CreditsButton")->show();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/Sheet")->show();
  
     CEGUI::WindowManager::getSingleton().getWindow("Ex1/QuitButton")->enable();
 CEGUI::WindowManager::getSingleton().getWindow("Ex1/RecordsButton")->enable();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/CreditsButton")->enable();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/Sheet")->enable();
  
    CEGUI::WindowManager::getSingleton().getWindow("Ex1/QuitButton")->captureInput();
 CEGUI::WindowManager::getSingleton().getWindow("Ex1/RecordsButton")->captureInput();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/CreditsButton")->captureInput();
  CEGUI::WindowManager::getSingleton().getWindow("Ex1/Sheet")->captureInput();
//   _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
//   createGUI();
  
  // iniciar musica
  
_mainTrack = _pTrackManager->load("menu_music.mp3");
  _mainTrack->play();

}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
  // Transición al siguiente estado.
//   // Espacio --> PlayState
//   if (e.key == OIS::KC_SPACE) {
//     pushState(PlayState::getSingletonPtr());
//   }
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
  if (e.key == OIS::KC_ESCAPE) {
      _exitGame = true;
  }
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

IntroState*
IntroState::getSingletonPtr ()
{
return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
