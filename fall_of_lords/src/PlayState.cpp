#include "PlayState.h"
#include "DotSceneLoader.h"
#define NAME_TEXTUTE_MINIMAP "RttT_Map"
#define NAME_MATERIAL_MINIMAP "RttMat_Map"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{  
  CEGUI::MouseCursor::getSingleton().hide();
  reset();
  //creating the screen instance and showning the loading screen
    GameManager::getSingletonPtr()->getLog()->logMessage("Entering play state");
  Ogre::Root *root = Ogre::Root::getSingletonPtr();
  _sceneMgr = root->createSceneManager(Ogre::ST_EXTERIOR_REAL_FAR,"PlayManager");
  
  Ogre::OverlayElement *elem;
   elem = Ogre::OverlayManager::getSingletonPtr()->getOverlayElement("Panel_Loading_Game");
    elem->setDimensions( GameManager::getSingletonPtr()->getViewport()->getActualWidth(), GameManager::getSingletonPtr()->getViewport()->getActualHeight());
    
    putOverlay ( "Loading_Game", true );
    
   GameManager::getSingletonPtr()->getRoot()->renderOneFrame();
 
   //loading the scene
   createInitialWorld();
  _camera = _sceneMgr->getCamera("Camera");

  Fader* fader = nullptr;
  fader = new Fader ( "GUI_Game", "Game/Recuadro_MiniMapa", NULL );
    _vFader.push_back ( fader );

  Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);


  
  _mainTrack = TrackManager::getSingletonPtr()->load("game_music.mp3");
  

  _overlayManager = Ogre::OverlayManager::getSingletonPtr();
  _overlayManager->getByName("GUI_Game")->show();

  _overlayManager->getByName("Info")->show();
 
   _sceneMgr->getSceneNode("puerta_metalica_abierta")->setVisible(false);

    /*creation of main user and camera*/
  mChar = new OgreCharacter(std::string("player_sword"), std::string("player_sword"), _sceneMgr, Ogre::Vector3(0,2,0), _world, MAX_HEALTH, MAX_MAGIC );
  mChar->setVisible(true);
 _characters.push_back(mChar);
 
 /*creation of enemies*/
//  for(int i=0;i<10;i++){
//   Character * enemy = new Enemy(_sceneMgr, _world, "bandit"+std::to_string(i), "bandit",Ogre::Vector3(4*i,0,4*i),mChar);
//  _characters.push_back(enemy);
//  _enemies.push_back(static_cast<Enemy*>(enemy));
//  }
  collocateEnemies();
  collocatePotions();

// Potion * potion = new Potion(_sceneMgr, _world, std::string("potion"), std::string("potion"),Ogre::Vector3(4, 0.82, 4),mChar, std::string("item.wav"),10);
//   _items.push_back(potion);
// Potion * potion = new Potion(_sceneMgr, _world, std::string("potion"), std::string("potion"),Ogre::Vector3(4, 0.82, 4),mChar, std::string("item.wav"),10);
//   _items.push_back(potion);

Key * key = new Key(_sceneMgr, _world, std::string("key"), std::string("key"),Ogre::Vector3(122, 2, -279),mChar,std::string("item.wav"));
  _items.push_back(key);  

     Ogre::ParticleSystem* phog = _sceneMgr->createParticleSystem("Phog","Examples/PurpleFountain");
//   Ogre::Entity* ent6 = _sceneMgr->createEntity("hoguera", "hoguera.mesh");
//   ent6->setCastShadows(true);
//   hoguera->scale(0.3,0.3,0.3);
//   hoguera->setPosition(-5,0,5);
//   hoguera->pitch(Ogre::Degree(90));
//   hoguera->attachObject(ent6);
   _sceneMgr->getSceneNode("key")->attachObject(phog);
//   _sceneMgr->getRootSceneNode()->addChild(hoguera);
// Ogre::ParticleSystem* phog = _sceneMgr->createParticleSystem("Phog","bonFire");
// _characters.push_back(potion);
// 
// potion = new Potion(_sceneMgr, _world, "potion1", "potion",Ogre::Vector3(-78, 0.82, -38),mChar);
// _characters.push_back(potion);



  mLord = new Lord(_sceneMgr, _world, "dark_lord", "death_knight_B",Ogre::Vector3(15, 0.82, -388),mChar);
// 
_characters.push_back(static_cast<Character*>(mLord)); _enemies.push_back(static_cast<Enemy*>(mLord));

//   _camera->setPosition(0,0,0);
  mExCamera = new ExtendedCamera("ExtendedCamera", _sceneMgr, _camera);
  //creation of world and minimap  
  createMiniMap();


  _last_time = _time_count = 0;
  _key_pressed= false;
  _exitGame=true;
  
  
   putOverlay ( "Loading_Game", false );
//   _mainTrack->play();
//    std::cout << "IS PLAYING : "<<_mainTrack->isPlaying() << std::endl;
// 
//   _mainTrack->play();

      
}

void PlayState::reset(){
  _mmCamera = nullptr;
  _mmNode = nullptr;
  _light= nullptr;
  mChar  = nullptr;
  mExCamera = nullptr;
  _world= nullptr;
  _staticGeometry= nullptr;
  _rigidTrack= nullptr;
  _characters.clear();
  _trackTrimesh= nullptr;
  _textureListener= nullptr;
  _rtex= nullptr;
  _vFader.clear();
  _items.clear();
  _enemies.clear();
  
}
void PlayState::createMiniMap() {
//     // Creamos la textura donde vamos a meter el mapa que va visualizarse a partir de la camara
    Ogre::TexturePtr _rtt = Ogre::TextureManager::getSingleton().createManual (
            NAME_TEXTUTE_MINIMAP, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
            Ogre::TEX_TYPE_2D, 256, 256, 0, Ogre::PF_A8R8G8B8, Ogre::TU_RENDERTARGET );
    _mmNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("CameraMiniMapNode");
    _mmCamera = _sceneMgr->createCamera ( "CameraMiniMap" );
    _mmNode->setAutoTracking(true, mChar->getMainNode());
    _mmNode->setFixedYawAxis(true);
    
    _mmCamera->setNearClipDistance(0.1);
    _mmCamera->setFarClipDistance(100);
    _mmNode->attachObject(_mmCamera);
    
    
    _rtex = _rtt->getBuffer()->getRenderTarget();
	  // Vinculamos la camara con la textura
    _rtex->addViewport ( _mmCamera );
    _rtex->getViewport(0)->setClearEveryFrame ( true );
    _rtex->getViewport(0)->setBackgroundColour ( Ogre::ColourValue::Black );
    _rtex->getViewport(0)->setOverlaysEnabled ( false );
    _rtex->setAutoUpdated(true);
    // Creamos el material al que le vamos a asginar la textura
    MaterialPtr mPtr = MaterialManager::getSingleton().create ( NAME_MATERIAL_MINIMAP, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
    Technique* matTechnique = mPtr->createTechnique();
    matTechnique->createPass();
    mPtr->getTechnique(0)->getPass(0)->setLightingEnabled(true);
    mPtr->getTechnique(0)->getPass(0)->setDiffuse(0.9,0.9,0.9,1);
    mPtr->getTechnique(0)->getPass(0)->setSelfIllumination(0.4,0.4,0.4);
    // Le asignamos la textura que hemos creado
    mPtr->getTechnique(0)->getPass(0)->createTextureUnitState(NAME_TEXTUTE_MINIMAP);

	  // Le vinculamos el listener a la textura
  	_textureListener = new MiniMapTextureListener (_sceneMgr ,   &_characters );
    _rtex->addListener ( _textureListener );

     Ogre::OverlayElement *elem;

	 // Asignamos el material al Overlay en el que mostramos el mapa
     elem = Ogre::OverlayManager::getSingletonPtr()->getOverlayElement("Panel_Mini_Map_Game");
     elem->setMaterialName ( NAME_MATERIAL_MINIMAP);

}

void 
PlayState::updateMiniMap(double timeSinceLastFrame){
// 	posHero.y = 1.0;

	// Situamos las camaras

  if(_mmNode && mChar){
    Ogre::Vector3 pos = mChar->getRigidBody()->getCenterOfMassPosition();
    Ogre::Vector3 cameraPosition = pos + Ogre::Vector3::UNIT_Z * DISTANCE_Z_CAMERA_MINIMAP + Ogre::Vector3::UNIT_Y * HIGHT_CAMERA_MINIMAP;
    Ogre::Vector3 displacement  = (cameraPosition - _mmNode->getPosition()) * timeSinceLastFrame;
    _mmNode->translate(displacement);
  }

}

void 
PlayState::deleteMiniMap(){
  if ( _textureListener )
      {
        _rtex->removeListener ( _textureListener );
        delete _textureListener;
      }

	  if ( _mmCamera )
    {
#ifdef _DEBUG
      cout << "delete camera" << endl;
#endif
      _sceneMgr->destroyCamera ( _mmCamera );
    }

	Ogre::TextureManager::getSingleton().remove(NAME_TEXTUTE_MINIMAP);

	Ogre::MaterialManager::getSingleton().remove(NAME_MATERIAL_MINIMAP);
	
	_mmNode->detachAllObjects();
	_mmNode->getCreator()->destroySceneNode(_mmNode);
	_mmCamera = nullptr;
	_mmNode = nullptr;
}


void 
PlayState::putOverlay (  std::string name, bool visible )
{
    Ogre::Overlay *over = nullptr;

    over =  Ogre::OverlayManager::getSingletonPtr()->getByName ( name );

    if ( over )
      {
        if ( visible )
            over->show();
        else
            over->hide();
      }
    else
      {
        cerr << "Overlay name was not found!!" << endl;
      }
}

void
PlayState::exit ()
{ 
  // Eliminar cuerpos rigidos --------------------------------------
//   std::deque <OgreBulletDynamics::RigidBody *>::iterator 
//      itBody = _bodies.begin();
//   while (_bodies.end() != itBody) {   
//     delete *itBody;  ++itBody;
//   } 
//  
//   // Eliminar formas de colision -----------------------------------
//   std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
//     itShape = _shapes.begin();
//   while (_shapes.end() != itShape) {   
//     delete *itShape; ++itShape;
//   } 
// 
//   _bodies.clear();  _shapes.clear();
  _mainTrack->stop();
 TrackManager::getSingletonPtr()->unload("menu_music.mp3");
  
  
  _sceneMgr->destroyLight(_light);
 putOverlay ( "GUI_Game", false );
          putOverlay ( "Info", false );
          putOverlay ( "Panel_Mini_Map_Game", false );
  deletePhysics();
  deleteMiniMap();
  
  //deleting items
  for(InteractiveObject* object: _items)
     delete object;
  _items.clear();
  
   delete mExCamera;
   
   //deleting characters 
   _enemies.clear();
   for(Character* character: _characters)
     delete character;
   _characters.clear();
   
   _mainTrack->stop();
     if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);

  CEGUI::MouseCursor::getSingleton().show();

}



void
PlayState::pause()
{  
  _mmNode->setVisible(false);
      putOverlay ( "GUI_Game", false );
          putOverlay ( "Info", false );
  _mainTrack->pause();
}

void
PlayState::resume()
{   
  putOverlay ( "GUI_Game", true );
  putOverlay ( "Info", true );
  Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  viewport->setCamera(_camera);
    _mmNode->setVisible(true);
  _mainTrack->play();
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  
  InputManager *inputManager = InputManager::getSingletonPtr();
  std::stringstream mines,remaining;
  deltaT = evt.timeSinceLastFrame;
  inputManager->getKeyboard()->capture();
  inputManager->getMouse()->capture();
//   float rotx = inputManager->getMouse()->getMouseState().X.rel * deltaT * -1;
//   float roty = inputManager->getMouse()->getMouseState().Y.rel * deltaT * -1;

  if (mChar) {
    mChar->update(Ogre::Real(evt.timeSinceLastFrame), inputManager->getKeyboard(), inputManager->getMouse());
  if (mExCamera) {
    switch (mExCamera->getMode()) {
      case 0: // 3rd person chase
	mExCamera->update(evt.timeSinceLastFrame,
	mChar->getCameraNode()->_getDerivedPosition(),
	mChar->getSightNode()->_getDerivedPosition());
      break;
    case 1: // 3rd person fixed
	mExCamera->update(Ogre::Real(evt.timeSinceLastFrame),
	Ogre::Vector3(0, 10, 0),
	mChar->getSightNode()->_getDerivedPosition());
    break;
    case 2: // 1st person
      mExCamera->update (evt.timeSinceLastFrame, 
                mChar->getWorldPosition (), 
                mChar->getSightNode ()->_getDerivedPosition());
	break;
	}
    }
    
       /* Tercera Persona */
    if (inputManager->getKeyboard()->isKeyDown(OIS::KC_F1)) {
      mExCamera->setMode(0);
      if (mChar)
	static_cast<OgreCharacter *>(mChar)->setVisible(true);
      if (mExCamera) {
	if (mChar){
	  mExCamera->instantUpdate(mChar->getCameraNode()->_getDerivedPosition(), mChar->getSightNode()->_getDerivedPosition());
	}
	mExCamera->setTightness(0.05f);
      }
    }
    /* Tercera Persona, CÃ¡mara Fija */
    if (inputManager->getKeyboard()->isKeyDown(OIS::KC_F2)) {
      mExCamera->setMode(1);
      if (mChar)
	static_cast<OgreCharacter *>(mChar)->setVisible(true);
	if (mExCamera) {
	  if (mChar){
	    mExCamera->instantUpdate(Ogre::Vector3(0, 50, 0), mChar->getSightNode()->_getDerivedPosition());
	  }
      mExCamera->setTightness(0.01f);
      }
    }
    /* Primera Persona */
    if (inputManager->getKeyboard()->isKeyDown(OIS::KC_F3))  {
      mExCamera->setMode(2);
	if (mChar)
	  static_cast<OgreCharacter *>(mChar)->setVisible(false);
	  if (mExCamera) {
	    if (mChar){
	      mExCamera->instantUpdate (mChar->getWorldPosition (), mChar->getSightNode ()->_getDerivedPosition());
                 mExCamera->setTightness (1.0f);
	  }
  }
}

 // Miramos si el heroe a encontrado a algun rehen
    Item* interactive = detectCollisionHeroWithItems( mChar, _items);
    if (interactive != nullptr) {
	interactive->effect();
	_items.remove(interactive);
	delete interactive;
	if(mChar->getKey())
	  doorOpen();
    }
  
 updatePanelLife();
 for(Character* enemy : _enemies)
    enemy->update(deltaT,inputManager->getKeyboard(), inputManager->getMouse());
 
 for(Item* item : _items)
    item->update(deltaT,inputManager->getKeyboard(), inputManager->getMouse());


 if(mChar->getHealth() <=0){
  //estamos en gameover
   //mostrar overlay
   pushState(GameOverState::getSingletonPtr());
 }
 
  if(mLord && mLord->getHealth() <=0){
   //Almacenar records
   std::ofstream file("records.txt", std::ofstream::app | std::ofstream::out);
  file << "Player " << StringConverter::toString(0) << std::endl;
  file.close();
   pushState(WinState::getSingletonPtr());
}
    }
  

  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{  
  
  if(_world)_world->stepSimulation(evt.timeSinceLastFrame,1);
  updateMiniMap(evt.timeSinceLastFrame);

  return _exitGame;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  if(!_mainTrack->isPlaying()) _mainTrack->play(true);
  _event = e.key;
  _key_pressed = true;
//   Tecla p --> PauseState.
  if (e.key == OIS::KC_L) {
    pushState(PauseState::getSingletonPtr());
  }
  else{
    if(e.key == OIS::KC_Q && mChar) mChar->swapRun2Walk();
    if(e.key == OIS::KC_S && mChar) mChar->turnFrontBack();
    if(e.key == OIS::KC_E && mChar) mChar->atack1Start();
    if(e.key == OIS::KC_SPACE && mChar)mChar->jumpStart();
//      if(e.key == OIS::KC_A && mChar)mChar->turn_left();
//       if(e.key == OIS::KC_D && mChar)mChar->turn_right();
    Ogre::Vector3 vt(0,0,0);     Ogre::Real tSpeed = 20.0;
    Ogre::Real deltaT = 0.03;
    
    
    if(e.key == OIS::KC_UP){ Ogre::OverlayElement *elem =_overlayManager->getOverlayElement("panelInfo"); elem->setMaterialName ( "Life1" );}
    if(e.key == OIS::KC_DOWN){doorClose();}
    if(e.key == OIS::KC_LEFT){doorOpen();}
    if(e.key == OIS::KC_RIGHT){mChar->setHealth(mChar->getHealth()-16); updatePanelLife();}
    
    if(mChar){
      if(mChar->isAttack()){
	  Enemy *obj = detectCollisionHeroWithEnemies(mChar, _enemies);
	  if(obj != nullptr){
	    obj->setHealth(obj->getHealth()-10);//TO MODIFY
// 	    obj->updateLifeBar();
	    if(obj->isDeath()){
	       SoundFXPtr sonido = SoundFXManager::getSingletonPtr()->load("explosion.wav");
	       
	       sonido->play();
	      _enemies.remove(obj);
	      _characters.remove(obj);
	      delete obj;
	    }
	    
	  }

      }
    }
      // 
//   if(e.key == OIS::KC_A)  _level->translate(Ogre::Vector3(6,0,0)*deltaT);
//   if(e.key == OIS::KC_D) _level->translate(Ogre::Vector3(6,0,0)*deltaT);
   
//     if(e.key == OIS::KC_I){ vt+=Ogre::Vector3(0,-1,0); _camera->moveRelative(vt * deltaT * tSpeed);}
//     if(e.key == OIS::KC_K){  vt+=Ogre::Vector3(0,1,0); _camera->yaw(Ogre::Radian(1 * deltaT * tSpeed));}
//     if(e.key == OIS::KC_J){  vt+=Ogre::Vector3(0,-1,0); _camera->yaw(Ogre::Radian(-1 * deltaT * tSpeed));}
//     if(e.key == OIS::KC_L){ vt+=Ogre::Vector3(0,1,0); _camera->moveRelative(vt * deltaT * tSpeed);}
//  
//     
//     if(e.key == OIS::KC_SPACE){
//       if(_end_game) popState();
//       else if(_level_finished) {
// 	_overlayManager->getOverlayElement("logoClear")->hide();
// 	_count_level++;
// 	delete _level;
// 	_puntuationAux += _level->getPuntuation();
// 	_level = new Level(_count_level,_difficult ,_sceneMgr);
// 	_level_finished = false;
//       } 
//     }
//     
  }
}


void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  _key_pressed = false;
  if (e.key == OIS::KC_ESCAPE) {
    popState();
  }
  
  if ((e.key == (OIS::KC_W) || e.key == OIS::KC_S || e.key == OIS::KC_A || e.key == OIS::KC_D || e.key == OIS::KC_R) && mChar) 
      mChar->stopAnimation();
  if (e.key == (OIS::KC_Q) && mChar) mChar->swapRun2Walk();
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
  
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
 

}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  

}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void 
PlayState::createInitialWorld(){
initPhysics();

 // Creacion del modulo de debug visual de Bullet ------------------
//     OgreBulletCollisions::DebugDrawer* _debugDrawer = new OgreBulletCollisions::DebugDrawer();
//   _debugDrawer->setDrawWireframe(true);	 
//   SceneNode *node = _sceneMgr->getRootSceneNode()->
//     createChildSceneNode("debugNode", Vector3::ZERO);
//   node->attachObject(static_cast <SimpleRenderable *>(_debugDrawer));
//     _world->setDebugDrawer (_debugDrawer);
//   _world->setShowDebugShapes (true);  // Muestra los collision shapes
// //   
   DotSceneLoader loader(_world);
  loader.parseDotScene("terraino.scene","General", _sceneMgr);

  //    // use a small amount of ambient lighting
   _sceneMgr->setAmbientLight(Ogre::ColourValue(0.3,0.3,0.3));

    //Sky
    _sceneMgr->setSkyBox(true, "3D-Diggers/SkyBox", 10);

 
  
  _light = _sceneMgr->createLight("Light1");
  _light->setType(Ogre::Light::LT_DIRECTIONAL);
  _light->setPosition(16, 80, -324);
  _light->setDirection(Ogre::Vector3(1,-1,0));
}

void PlayState::initPhysics(){
   Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
    Ogre::Vector3 (-10000, -10000, -10000), 
    Ogre::Vector3 (10000,  10000,  10000));
  Ogre::Vector3 gravity = Vector3(0, -9.8, 0);
   _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
 	   worldBounds, gravity);

}

void PlayState::deletePhysics(){  
  delete _rigidTrack; delete _trackTrimesh;
  delete _world;
}

Enemy* PlayState::detectCollisionHeroWithEnemies(OgreCharacter* hero, std::list<Enemy*> enemies) {
	bool isCollition = false;
	Enemy* enemyCollisition = nullptr;
	btCollisionWorld *bulletWorld = hero->getWorld()->getBulletCollisionWorld();
	int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();
	btPersistentManifold* contactManifold = nullptr;
	btCollisionObject* obA;
	btCollisionObject* obB;

	// Recorremos las colisiones que se esten produciendo
	for (int i=0;i<numManifolds && !isCollition;i++) {
		contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
		obA = (btCollisionObject*)(contactManifold->getBody0());
		obB = (btCollisionObject*)(contactManifold->getBody1());

		OgreBulletCollisions::Object* obOB_A = hero->getWorld()->findObject(obA);
		OgreBulletCollisions::Object* obOB_B = hero->getWorld()->findObject(obB);

		if (obOB_A == hero->getRigidBody() || obOB_B == hero->getRigidBody()) {
			for (Enemy *enemy: enemies) {
				isCollition = (obOB_A == enemy->getRigidBody() || obOB_B == enemy->getRigidBody());
				if (isCollition){
				  enemyCollisition = enemy;
				  std::cout << "COLISION" << std::endl;
				  break;
				}
			}
		}
	  if(isCollition) break;

	}
	return enemyCollisition;
}

Item* PlayState::detectCollisionHeroWithItems(OgreCharacter* hero, std::list<Item*> items) {
	bool isCollition = false;
	Item* itemCollisition = nullptr;
	btCollisionWorld *bulletWorld = hero->getWorld()->getBulletCollisionWorld();
	int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();
	btPersistentManifold* contactManifold = nullptr;
	btCollisionObject* obA;
	btCollisionObject* obB;

	// Recorremos las colisiones que se esten produciendo
	for (int i=0;i<numManifolds && !isCollition;i++) {
		contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
		obA = (btCollisionObject*)(contactManifold->getBody0());
		obB = (btCollisionObject*)(contactManifold->getBody1());

		OgreBulletCollisions::Object* obOB_A = hero->getWorld()->findObject(obA);
		OgreBulletCollisions::Object* obOB_B = hero->getWorld()->findObject(obB);

		if (obOB_A == hero->getRigidBody() || obOB_B == hero->getRigidBody()) {
			for (Item *item: items) {
				isCollition = (obOB_A == item->getRigidBody() || obOB_B == item->getRigidBody());
				if (isCollition){
				  itemCollisition = item;
				  std::cout << "COLISION" << std::endl;
				  break;
				}
			}
		}
	  if(isCollition) break;

	}
	return itemCollisition;
}

void PlayState::updatePanelLife()
  {
  if(mChar){
    Ogre::OverlayElement *elem = NULL;

//     if ( currentPanelLife != _lastPanelLife )
//       {

        elem = _overlayManager->getOverlayElement("panelInfo");
   

        if ( mChar->getHealth() > 84 ) {
	  elem->setMaterialName ( "Life" );
        }
        else if ( mChar->getHealth() > 68 && mChar->getHealth() <= 84 ) {
      elem->setMaterialName ( "Life1" );
        }
        else if ( mChar->getHealth() > 52 && mChar->getHealth() <= 68 ) {
      elem->setMaterialName ( "Life2" );
        }
         else if ( mChar->getHealth() > 36 && mChar->getHealth() <= 52 ) {
      elem->setMaterialName ( "Life3" );
        }
         else if ( mChar->getHealth() > 20 && mChar->getHealth() <= 36 ) {
      elem->setMaterialName ( "Life4" );
        }
        else if ( mChar->getHealth() > 4 && mChar->getHealth() <= 20 ) {
      elem->setMaterialName ( "Life5" );
        }
        else {
         elem->setMaterialName ( "Life6" );
        }
  }
  
  }
  
void PlayState::doorOpen(){
  
   SceneNode* puerta_cerrada = _sceneMgr->getSceneNode("puerta_metalica_cerrada");


   OgreBulletDynamics::RigidBody* rigidbody= static_cast<OgreBulletDynamics::RigidBody*>(_world->findObject(puerta_cerrada));
   rigidbody->setKinematicObject(false);
   rigidbody->getBulletRigidBody()->setActivationState(false);
   _world->getBulletDynamicsWorld()->removeCollisionObject(rigidbody->getBulletObject());
   puerta_cerrada->setVisible(false);
   
   puerta_cerrada = _sceneMgr->getSceneNode("puerta_metalica_abierta");
   
   rigidbody= static_cast<OgreBulletDynamics::RigidBody*>(_world->findObject(puerta_cerrada));
   rigidbody->setKinematicObject(true);
   rigidbody->getBulletRigidBody()->setActivationState(true);
   _world->getBulletDynamicsWorld()->addCollisionObject(rigidbody->getBulletObject());
      puerta_cerrada->setVisible(true);

   
   
}

void PlayState::doorClose(){
  
 SceneNode* puerta_cerrada = _sceneMgr->getSceneNode("puerta_metalica_cerrada");


   OgreBulletDynamics::RigidBody* rigidbody= static_cast<OgreBulletDynamics::RigidBody*>(_world->findObject(puerta_cerrada));
   rigidbody->setKinematicObject(true);
   rigidbody->getBulletRigidBody()->setActivationState(true);
   _world->getBulletDynamicsWorld()->addCollisionObject(rigidbody->getBulletObject());
   puerta_cerrada->setVisible(true);
   
   puerta_cerrada = _sceneMgr->getSceneNode("puerta_metalica_abierta");
   
   rigidbody= static_cast<OgreBulletDynamics::RigidBody*>(_world->findObject(puerta_cerrada));
    rigidbody->setKinematicObject(false);
   rigidbody->getBulletRigidBody()->setActivationState(false);
//    _world->getBulletDynamicsWorld()->removeCollisionObject(rigidbody->getBulletObject());
      puerta_cerrada->setVisible(false);

}

void PlayState::collocateEnemies(){
//    Character * enemy  = new Enemy(_sceneMgr, _world, "bandit", "bandit",Ogre::Vector3(-90, 0, -38),mChar);
//  _characters.push_back(enemy); _enemies.push_back(static_cast<Enemy*>(enemy));
 
 Character * enemy = new Enemy(_sceneMgr, _world, "bandit3", "bandit",Ogre::Vector3(-62, 0, -38),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit4", "bandit",Ogre::Vector3(-93, 0, -103),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit5", "bandit",Ogre::Vector3(-47, 0, 9),mChar);
 _characters.push_back(enemy); _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit6", "bandit",Ogre::Vector3(-88, 0, -117),mChar);
 _characters.push_back(enemy); _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit8", "bandit",Ogre::Vector3(-81, 0, -187),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit9", "bandit",Ogre::Vector3(-63, 0, -187),mChar);
 _characters.push_back(enemy); _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit10", "bandit",Ogre::Vector3(-72, 0, -203),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit11", "bandit",Ogre::Vector3(16, 0, -239),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit12", "bandit",Ogre::Vector3(28,0, -248),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit13", "bandit",Ogre::Vector3(84, 0, -206),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit14", "bandit",Ogre::Vector3(1,0,-300),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

 enemy = new Enemy(_sceneMgr, _world, "bandit15", "bandit",Ogre::Vector3(56,0, -222),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));
 
  enemy = new Enemy(_sceneMgr, _world, "bandit16", "bandit",Ogre::Vector3(103,0, 10),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));
 
  enemy = new Enemy(_sceneMgr, _world, "bandit17", "bandit",Ogre::Vector3(149,16, -63),mChar);
 _characters.push_back(enemy);  _enemies.push_back(static_cast<Enemy*>(enemy));

}

void PlayState::collocatePotions(){
  Potion * potion = new Potion(_sceneMgr, _world, std::string("potion1"), std::string("potion"),Ogre::Vector3(4, 0.82, 4),mChar, std::string("item.wav"),10);
  _items.push_back(potion);
 
 potion = new Potion(_sceneMgr, _world, "potion2", "potion",Ogre::Vector3(-90,3, 14),mChar,std::string("item.wav"),20);
 _items.push_back(potion);  

 potion = new Potion(_sceneMgr, _world, "potion3", "potion",Ogre::Vector3(-78, 3, -76),mChar,std::string("item.wav"),15);
 _items.push_back(potion);  
 potion = new Potion(_sceneMgr, _world, "potion4", "potion",Ogre::Vector3(-56, 3, 24),mChar,std::string("item.wav"),60);
 _items.push_back(potion);  
 potion = new Potion(_sceneMgr, _world, "potion5", "potion",Ogre::Vector3(-13, 3, -20),mChar,std::string("item.wav"),40);
 _items.push_back(potion);

 potion = new Potion(_sceneMgr, _world, "potion6", "potion",Ogre::Vector3(22, 3, -17),mChar,std::string("item.wav"),50);
 _items.push_back(potion); 

 potion = new Potion(_sceneMgr, _world, "potion7", "potion",Ogre::Vector3(-127, 3, -227),mChar,std::string("item.wav"),20);
 _items.push_back(potion); 

 potion = new Potion(_sceneMgr, _world, "potion8", "potion",Ogre::Vector3(-13, 3, -223),mChar,std::string("item.wav"),16);
 _items.push_back(potion);  
 potion = new Potion(_sceneMgr, _world, "potion9", "potion",Ogre::Vector3(-6, 3, -360),mChar,std::string("item.wav"),40);
 _items.push_back(potion); 

 potion = new Potion(_sceneMgr, _world, "potion10", "potion",Ogre::Vector3(35, 3, -357),mChar,std::string("item.wav"),10);
 _items.push_back(potion);  

 potion = new Potion(_sceneMgr, _world, "potion11", "potion",Ogre::Vector3(54, 3, -260),mChar,std::string("item.wav"),80);
 _items.push_back(potion);  
 
  potion = new Potion(_sceneMgr, _world, "potion12", "potion",Ogre::Vector3(-34, 3, -282),mChar,std::string("item.wav"),60);
 _items.push_back(potion);  
 
  potion = new Potion(_sceneMgr, _world, "potion13", "potion",Ogre::Vector3(-6, 3, -362),mChar,std::string("item.wav"),50);
 _items.push_back(potion);  
 
  potion = new Potion(_sceneMgr, _world, "potion14", "potion",Ogre::Vector3(25, 3, -362),mChar,std::string("item.wav"),80);
 _items.push_back(potion);  
 
   potion = new Potion(_sceneMgr, _world, "potion15", "potion",Ogre::Vector3(38, 3, -377),mChar,std::string("item.wav"),100);
 _items.push_back(potion);  
 
  potion = new Potion(_sceneMgr, _world, "potion16", "potion",Ogre::Vector3(-8, 3, -386),mChar,std::string("item.wav"),100);
 _items.push_back(potion);  
 
   potion = new Potion(_sceneMgr, _world, "potion17", "potion",Ogre::Vector3(-106, 3, -181),mChar,std::string("item.wav"),100);
 _items.push_back(potion);  
 
    potion = new Potion(_sceneMgr, _world, "potion18", "potion",Ogre::Vector3(49, 3, 206),mChar,std::string("item.wav"),10);
 _items.push_back(potion);

    potion = new Potion(_sceneMgr, _world, "potion19", "potion",Ogre::Vector3(-133, 3, -273),mChar,std::string("item.wav"),100);
 _items.push_back(potion);
 
    potion = new Potion(_sceneMgr, _world, "potion20", "potion",Ogre::Vector3(114, 3, 49),mChar,std::string("item.wav"),40);
 _items.push_back(potion); 
 
  potion = new Potion(_sceneMgr, _world, "potion21", "potion",Ogre::Vector3(137, 8, -121),mChar,std::string("item.wav"),100);
 _items.push_back(potion);

}

