 
#include "WinState.h"

template<> WinState* Ogre::Singleton<WinState>::msSingleton = 0;


void
WinState::enter ()
{    std::cout << "Win state enter"<<std::endl;

   Ogre::Root* root = Ogre::Root::getSingletonPtr();
  _sceneMgr = root->createSceneManager(Ogre::ST_EXTERIOR_REAL_FAR,"WinManager");
  _camera = _sceneMgr->createCamera("WinCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
  
  Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);
  
  _exitGame=false;
  _overlay = Ogre::OverlayManager::getSingletonPtr()->getByName("GUI_Win");
  _overlay->show();
  _win = SoundFXManager::getSingletonPtr()->load("win.wav");
  _win->play();
}

void
WinState::exit ()
{
//     CEGUI::WindowManager::getSingleton().getWindow("Pause")->hide();
//      CEGUI::WindowManager::getSingleton().getWindow("Pause")->deactivate();
//      CEGUI::WindowManager::getSingleton().getWindow("Pause")->releaseInput();
  _sceneMgr->destroyCamera(_camera);
  
    if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);
  _overlay->hide();
}

void
WinState::pause ()
{
}

void
WinState::resume ()
{
}

bool
WinState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
WinState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
WinState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_SPACE) {
    popAllStatesAndPushState(IntroState::getSingletonPtr()); 
  }
}

void
WinState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
WinState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
WinState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
WinState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

WinState*
WinState::getSingletonPtr ()
{
return msSingleton;
}

WinState&
WinState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
