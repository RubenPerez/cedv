#include "GameOverState.h"

template<> GameOverState* Ogre::Singleton<GameOverState>::msSingleton = 0;


void
GameOverState::enter ()
{    std::cout << "Game over state enter"<<std::endl;

   Ogre::Root* root = Ogre::Root::getSingletonPtr();
  _sceneMgr = root->createSceneManager(Ogre::ST_EXTERIOR_REAL_FAR,"GameOverManager");
  _camera = _sceneMgr->createCamera("GameOverCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
  
  Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);
  
  _exitGame=false;
  _overlay = Ogre::OverlayManager::getSingletonPtr()->getByName("GUI_GameOver");
  _overlay->show();
  _gameover = SoundFXManager::getSingletonPtr()->load("gameover.wav");
  _gameover->play();
}

void
GameOverState::exit ()
{
//     CEGUI::WindowManager::getSingleton().getWindow("Pause")->hide();
//      CEGUI::WindowManager::getSingleton().getWindow("Pause")->deactivate();
//      CEGUI::WindowManager::getSingleton().getWindow("Pause")->releaseInput();
  _sceneMgr->destroyCamera(_camera);
  
    if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);
  _overlay->hide();
}

void
GameOverState::pause ()
{
}

void
GameOverState::resume ()
{
}

bool
GameOverState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
GameOverState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
GameOverState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_SPACE) {
    popAllStatesAndPushState(IntroState::getSingletonPtr()); 
  }
}

void
GameOverState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
GameOverState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
GameOverState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
GameOverState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

GameOverState*
GameOverState::getSingletonPtr ()
{
return msSingleton;
}

GameOverState&
GameOverState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
 
