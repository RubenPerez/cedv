#include "Character.h"
#define SIZE_LIFE_BAR 2.0f //quitar

Character::Character(){
    _nodeDummy = nullptr;
    _entityDummy = nullptr;
    _status = Status::Idle;
    _life = MAX_HEALTH;
}

Character::Character ( Ogre::SceneManager* sceneMgr,
                        OgreBulletDynamics::DynamicsWorld* world,
                        const std::string& name, const std::string& mesh,
                        const Ogre::Vector3& v_pos ) :   InteractiveObject(sceneMgr,world,name,mesh,v_pos) 
{
    _nodeDummy = nullptr;
    _entityDummy = nullptr;
    _status = Status::Idle;
    _life = MAX_HEALTH;
    _magic=nullptr;

 
    //_entityDummy
    _entityDummy = sceneMgr->createEntity ( _mName+ "DUMMY", "CubeEnemy.mesh"  );
    _entityDummy->setCastShadows(false);
    _entityDummy->setVisible(false);
    
    _nodeDummy = _mMainNode->createChildSceneNode ( _mName + "DUMMY" );
    _nodeDummy->setPosition(0, (_entityDummy->getBoundingBox().getSize().y / -2), 0);
    _nodeDummy->attachObject ( _entityDummy );
   
 }

Character::~Character()
{

      if(_nodeDummy) _nodeDummy->detachAllObjects();
    
      if(_magic) _mSceneMgr->destroyParticleSystem(_magic);
      if(_nodeDummy) _mSceneMgr->destroySceneNode(_nodeDummy);
      _mSceneMgr->destroyEntity(_entityDummy);

}

Character::Character ( const Character& other )
  {
  copy(other);
  }

Character& Character::operator=(const Character& rhs)
  {
    if (this == &rhs) return *this; // handle self assignment
    copy(rhs);
    return *this;
  }

  
void Character::copy ( const Character& rhs )
{
//       _mName = rhs.getName();
// 	_status = rhs.getStatus();
//     _life = rhs.getHealth();
//     _mMainNode = rhs.getMainNode();
//     _mEntity = rhs.getMainEntity();
//     _mSceneMgr = rhs.getSceneManager();
//     _rigidBody = rhs.getRigidBody();
//     _mAnimation = rhs.getAnimation();
//     _nodeDummy = rhs.getDummyNode();
//     _lifeNode = rhs.getLifeNode();
//     _bbSetLife = rhs.getBillBoardSet();
//     _lifeBar = rhs.getLifeBar();
//     _magic = rhs.getMagic();
//     _sceneBoxShape = rhs.getCollisionShape();
//     _entityDummy = rhs.getDummyEntity();
//     _visible = rhs.isVisible();
//     _world = rhs.getWorld();
//     _life = rhs.getHealth();
//     _level = rhs.getLevel();
//     _visible = rhs.isVisible();
}



  
  
  
SceneNode*
Character::getDummyNode() const{
  return _nodeDummy;
}


Ogre::Entity*
Character::getDummyEntity() const{
  return _entityDummy;
}


Status
Character::getStatus() const{
  return _status;
}



Ogre::ParticleSystem*
Character::getMagic() const{
  return _magic;
}

bool
Character::isDeath() const{
  return _life<=0;
}

void 
Character::setHealth(int life) {
  this->_life = life;
}

int 
Character::getHealth() const{
    return _life;
}

int 
Character::getLevel() const{
  return _level;
}