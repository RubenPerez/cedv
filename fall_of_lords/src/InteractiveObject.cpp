
#include "InteractiveObject.h"

InteractiveObject::InteractiveObject(){
   _mMainNode = nullptr;
    _rigidBody = nullptr;
    _sceneBoxShape= nullptr;
    _visible = true;
    _world = nullptr;
    _mAnimation=nullptr;
    _mSceneMgr = nullptr;
    _visible=true;
    _stateObject = LIVE;
}
    
  InteractiveObject::InteractiveObject(Ogre::SceneManager* sceneMgr,
                OgreBulletDynamics::DynamicsWorld* world,
                const String& name, const String& mesh,
                const Ogre::Vector3& v_pos):_mSceneMgr(sceneMgr),_world(world),_mName(name),_visible(true),
                _position(v_pos){
      _stateObject = LIVE;

		  //entity
    _mMainNode= _mSceneMgr->createSceneNode(name);
    _mMainNode->setPosition(v_pos);
    _mEntity = _mSceneMgr->createEntity(_mName, mesh+".mesh");
    _mEntity->setCastShadows(true);
    _mEntity->setVisible(_visible);
    _mMainNode->attachObject(_mEntity);
    _mSceneMgr->getRootSceneNode()->addChild(_mMainNode);
    
     //animation
    if(_mEntity->hasSkeleton())
      _mAnimation = new AnimationBlender(_mEntity);
    else _mAnimation = nullptr;
//     _mAnimation->blend("Idle", AnimationBlender::Switch, 0.5, true);
    
    //physic
    OgreBulletCollisions::StaticMeshToShapeConverter* trimeshConverter = new
    OgreBulletCollisions::StaticMeshToShapeConverter ( _mEntity );
    _sceneBoxShape = (OgreBulletCollisions::CollisionShape*) trimeshConverter->createConvex();  
    delete trimeshConverter;
  
    
    _rigidBody = new OgreBulletDynamics::RigidBody ( _mName, _world );
    
    _rigidBody->setShape ( _mMainNode, _sceneBoxShape, 0.2, 0.8, 75.0f,
			   v_pos, _mMainNode->_getDerivedOrientation() );
    _rigidBody->getBulletRigidBody()->setAngularFactor(0.0f);
  _world->getBulletDynamicsWorld()->addCollisionObject(_rigidBody->getBulletObject());
  
    
  }
  
   InteractiveObject::~InteractiveObject(){
           if(_mMainNode)_mMainNode->detachAllObjects();
      if(_mAnimation) delete _mAnimation;
      _visible = false;
  _rigidBody->setKinematicObject(false);
      _rigidBody->getBulletRigidBody()->setActivationState(false);
      _mSceneMgr->destroyEntity(_mEntity);

      _world = nullptr;
   }
  
  SceneNode *
  InteractiveObject::getMainNode() const{
    return _mMainNode;
  }
  
  Ogre::Entity* 
  InteractiveObject::getMainEntity() const{
     return _mEntity;
  }
  
  SceneManager * 
  InteractiveObject::getSceneManager() const{
      return _mSceneMgr;
  }
  
  AnimationBlender * 
  InteractiveObject::getAnimation() const{
      return _mAnimation;
  }
  
  OgreBulletDynamics::RigidBody * 
  InteractiveObject::getRigidBody() const{
    return _rigidBody;
  }
  
  OgreBulletCollisions::CollisionShape * 
  InteractiveObject::getCollisionShape() const{
      return _sceneBoxShape;
  }
  
  Ogre::Vector3  
  InteractiveObject::getPosition() const{
    return _position;
  }
  
  OgreBulletDynamics::DynamicsWorld * 
  InteractiveObject::getWorld() const{
      return _world;
  }
  
  Vector3
    InteractiveObject::getWorldPosition () const{
             return _mMainNode->_getDerivedPosition ();
         }
         
std::string 
InteractiveObject::getName() const
{
  return this->_mName;
}

bool
InteractiveObject::isVisible() const{
  return _visible;
}
  
  void 
  InteractiveObject::turn_angle ( const Ogre::Radian& angle ){
  _rigidBody->enableActiveState();
		    _mMainNode->yaw ( angle );
		    btQuaternion quaternion = OgreBulletCollisions::OgreBtConverter::to ( _mMainNode->getOrientation() );
		    _rigidBody->getBulletRigidBody()->getWorldTransform().setRotation ( quaternion );
    
  }
  
  void 
  InteractiveObject::turn_left (  ){
    turn_angle ( Ogre::Radian ( Ogre::Math::HALF_PI / 32 ) );
  }
  
  void 
  InteractiveObject::turn_right (  ){
    turn_angle ( Ogre::Radian ( (-1) * Ogre::Math::HALF_PI / 32 ) );
  }