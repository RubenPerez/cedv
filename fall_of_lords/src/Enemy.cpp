#include "Enemy.h"

static bool forcedIN = false;


Enemy::Enemy( Ogre::SceneManager* sceneMgr,
                    OgreBulletDynamics::DynamicsWorld* world,
                    const std::string& name, const std::string& mesh,
                    const Ogre::Vector3& v_pos,
                    Character* ptrHero ) : Character ( sceneMgr,
                                                        world,
                                                        name, mesh,
                                                        v_pos)
  {
    _particleDeath  = nullptr;
    _particleDeathNode=nullptr;
 _timerParticleDeath=0;

    _currentPosition = Ogre::Vector3::ZERO;
    _refHero = ptrHero;
    _animated = false;
    //Material del enemigo
    _entityDummy->setMaterialName ( "MaterialRojo" );
    _currentAnimation = nullptr;
   
    // Cargamos la ruta asociada al enemigo por donde se va a mover
  this->createLifeBar();
  _particleDeath = _mSceneMgr->createParticleSystem("particleEnemy" + _mName, "particleEnemy");
	// Creamos un nodo
	_particleDeathNode = _mMainNode->createChildSceneNode("particleDeathNode" + _mName);
	// Ajuntamos las partículas al nodo
	_particleDeathNode->attachObject(_particleDeath);
    _particleDeathNode->setVisible(false);
    _route =  EnemyRoute(v_pos, 10);


    _currentState = WATCHING;

//     _soundAlert1FX = SoundFXManager::getSingleton().load("alert1.wav");
//     _soundAlert2FX = SoundFXManager::getSingleton().load("alert2.wav");

//     _currentSoundAlert = 1;
//         		changeAnimation(STOP_ANIMATION);
   _currentAnimation = (_mEntity)->getAnimationState(STOP_ANIMATION);
        _currentAnimation->setEnabled(true);
        _currentAnimation->setLoop(true);
		 _currentAnimation->setWeight(1);
    _currentAnimation->setTimePosition(0);
    
    
     // Textura para mostrar la visualizacion de lo que ve el enemigo
//     _rtt = Ogre::TextureManager::getSingleton().createManual (
//             NAME_TEXTUTE_CAM + name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
//             Ogre::TEX_TYPE_2D, 64, 64, 0, Ogre::PF_A8R8G8B8, Ogre::TU_RENDERTARGET );
// 
//     _rtex = _rtt->getBuffer()->getRenderTarget();
//     // Camara de lo que visualiza el enemigo
//     _camPOV = sceneMgr->createCamera ( "cameraPOV_" + name );
//     _camPOV->setPosition ( Ogre::Vector3 ( 0, 0, 0.2 ) );
//     _camPOV->lookAt ( Ogre::Vector3 ( 0, 0, 5 ) );
//     _camPOV->setNearClipDistance ( 0.1 );
//     _camPOV->setFOVy ( Ogre::Degree ( 90 ) );
//     Ogre::SceneNode *nodeCamera = _mMainNode->createChildSceneNode ( "nodeCameraPOV_" + name );
// 	  nodeCamera->attachObject(_camPOV);
//     // Vinculamos la textura con la camara del enemigo
//     _rtex->addViewport ( _camPOV );
//     _rtex->getViewport(0)->setClearEveryFrame ( true );
//     _rtex->getViewport(0)->setBackgroundColour ( Ogre::ColourValue::Black );
//     _rtex->getViewport(0)->setOverlaysEnabled ( false );
//     _rtex->setAutoUpdated(true);
//     
//      // Para poner al personaje principal en material blanco y poder verlo el enemigo
//     _textureListener = new MyTextureListener ( sceneMgr, _rtt );
//     _rtex->addListener ( _textureListener );
  }
  
void
Enemy::createLifeBar(){
   _bbSetLife = _mSceneMgr->createBillboardSet();
    _bbSetLife->setMaterialName("lifeBar");
    _lifeBar = _bbSetLife->createBillboard(Ogre::Vector3(0, 4, 0));
    _lifeBar->setDimensions(1, 0.1);
    _lifeBar->setTexcoordRect(0.0f, 0.0f, 0.5f, 1.0f);
    
    _lifeNode = _mMainNode->createChildSceneNode();

    _lifeNode->attachObject(_bbSetLife);
        _lifeNode->setVisible(true);

 }
 


 void 
 Enemy::setVisible (bool visible) {
   _visible = visible;
   _mMainNode->setVisible (visible);
   if (visible) {
      _world->getBulletDynamicsWorld()->addCollisionObject(_rigidBody->getBulletObject());
    } else {
      _world->getBulletDynamicsWorld()->removeCollisionObject(_rigidBody->getBulletObject());
  }
     
 }
Enemy::~Enemy()
  {
    
//      if ( _textureListener )
//       {
//         _rtex->removeListener ( _textureListener );
//         delete _textureListener;
//       }
// 
//     if ( _camPOV )
//       _mSceneMgr->destroyCamera ( _camPOV );
    

       _particleDeathNode->detachAllObjects();
	_mSceneMgr->destroySceneNode(_particleDeathNode);
	_mSceneMgr->destroyParticleSystem(_particleDeath);
	
	  if(_lifeNode && _bbSetLife)_lifeNode->detachObject(_bbSetLife);
      if(_bbSetLife)_mSceneMgr->destroyBillboardSet(_bbSetLife);
      if(_lifeNode)_mSceneMgr->destroySceneNode(_lifeNode);
      

    Ogre::MaterialManager::getSingleton().remove(NAME_TEXTUTE_CAM + _mName);

  }


bool Enemy::haveYouSeenAnybody()
  {
//     assert ( _textureListener );
//     return _textureListener->enemyViewed();
       Ogre::Vector3 p = _refHero->getMainNode()->getPosition();

    Ogre::Vector3 o = _rigidBody->getSceneNode()->getPosition();
/*
    Ogre::Vector3 v = p - o; // 1er vector

    Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z; // 2do vector*/
       
// return _textureListener->enemyViewed();
    return p.distance(o) < VISION_DISTANCE;
    
  }



void /*Enemy::update ( double timeSinceLastFrame, std::vector<Character*>   vCharacteres)*/
Enemy::update(float timeSinceLastFrame, OIS::Keyboard const*, OIS::Mouse const*)
  {
    
if (_stateObject != END) {
      if (_stateObject == DEAD) {
			_timerParticleDeath += timeSinceLastFrame;
			_particleDeath->setEmitting(true);
			if (_timerParticleDeath > TIMER_PATICLE_DEATH) {
				_particleDeath->setEmitting(false);
				_stateObject = END;
			}
		} else {
			if ( _currentAnimation != nullptr )
			{
				_currentAnimation->addTime(timeSinceLastFrame );
			}

// 			if (_nodeShot && _isShooting) {
// 				updateShot(timeSinceLastFrame, vCharacteres);
// 			}

			_timerParticleDeath = 0.0;
    }
}
  
     if ( _stateObject == LIVE )
      {
//         _timeElapsed_Global += timeSinceLastFrame;

        updateLifeBar();

        // Cambios de estado del enemigo

        switch ( _currentState )
          {
            // ################## ESTADO WATCHING ##################
            // Si estamos en el estado normal de WATCHING
            case WATCHING:
                // Si avistamos al heroe :
                // - tomamos su posiciÃ³n actual
                // - cogemos el tiempo actual
                // - pasamos al estado ALERT CHASING
	         changeAnimation(STOP_ANIMATION);

                if ( haveYouSeenAnybody() )
                  {
//                     _positionLastViewed = _refHero->getPosition();
//                     _timeFirstVision = _timeElapsed_Global;
                    setCurrentState ( CHASING );
                  }
                // Si no vemos al heroe :
                // - reseteamos a posicion inicial
                
                else
                  {
// 		    walk_to(_route.getNextPoint(_mMainNode->getPosition()), false);//////////////////
                      ;
                  }

                break;

	    case ATTACK:
	      if ( isInContact() ){
		   reorient_enemy_to_hero();
		   attack(timeSinceLastFrame);	  
	      }
	       setCurrentState(CHASING);
	      break;
	      
	    case CHASING:
	      
	      if(haveYouSeenAnybody() ) {
		reorient_enemy_to_hero();
		if(isInContact()){
		  setCurrentState(ATTACK);
		}
		else{
		  walk_to(_refHero->getMainNode()->getPosition(), timeSinceLastFrame,true);;
		  ;
		}
	      }
	      else
		 {
		   _timeLostVision +=timeSinceLastFrame;
		   if(_timeLostVision >= 5){
		     setCurrentState(WATCHING); _timeLostVision=0;}
		     
		   else{
		     walk_to(_route.getCenter() ,timeSinceLastFrame,false);
		   }
// 		   if(_mMainNode->getPosition() != _route.getCenter())
// 		    walk_to(_route.getCenter() ,timeSinceLastFrame,false);
// 		   else
// 		     setCurrentState(WATCHING);
                  }
		
	    break;
	      
    }
  }
//      _currentAnimation->addTime(timeSinceLastFrame*100);

  }
// void Enemy::play_sound_alert()
// {
//   if ( _currentSoundAlert == 1 )
//     {
//       _soundAlert1FX->play();
//       _currentSoundAlert = 2;
//     }
//   else
//     {
//       _soundAlert2FX->play();
//       _currentSoundAlert = 1;
//     }
//   }
// 
// const Ogre::Real& Enemy::get_distance_with_hero()
//   {
//     Ogre::Vector3 p = _refHero->getSceneNode()->getPosition();
//     Ogre::Vector3 o = _node->getPosition();
// 
//     Ogre::Real distance = o.distance ( p );
// 
//     return distance;
//   }

bool Enemy::walk_to ( const Ogre::Vector3& p,  float timeSinceLastFrame ,bool running)
  {
   
    Ogre::Vector3 o = _rigidBody->getSceneNode()->getPosition();
    
    if(o == p) return true;

    Ogre::Vector3 v = p - o; // 1er vector

    Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z; // 2do vector

//     Ogre::Radian angle = orientacion.angleBetween ( v );

//     Ogre::Real /*distance*/ = o.distance ( p );


    if ( orientacion.getRotationTo(v).getYaw().valueDegrees() > 0 )
      {
        if ( orientacion.getRotationTo(v).getYaw().valueDegrees() > 170 )
          {
            turn_left(); turn_left();
          }
        else if ( orientacion.getRotationTo(v).getYaw().valueDegrees() > 10 )
          {
            turn_left();
          }
        else if ( orientacion.getRotationTo(v).getYaw().valueDegrees() > 0.1 )
          {
            turn_angle ( orientacion.getRotationTo(v).getYaw() );
          }
      }
      else
      {
        if ( orientacion.getRotationTo(v).getYaw().valueDegrees() < -170 )
          {
            turn_right(); turn_right();
          }
        else if ( orientacion.getRotationTo(v).getYaw().valueDegrees() < -10 )
          {
            turn_right();
          }
        else if ( orientacion.getRotationTo(v).getYaw().valueDegrees() < -0.1 )
          {
            turn_angle ( orientacion.getRotationTo(v).getYaw() );
          }
      }

    if ( running )
      run ( false, 200 , timeSinceLastFrame);
    else
      walk ( false, 100, timeSinceLastFrame );

    return false;
  }


void Enemy::showDummy(bool show)
  {
//     Character::showDummy(show);
    _lifeNode->setVisible(!show);
       this->getMainEntity()->setVisible(!show);
      this->getDummyEntity()->setVisible(show);
  }
// 
void Enemy::setCurrentState( const STATES_ENEMY& newValue )
  {
    std::string msg = "";

    _currentState = newValue;

    switch ( _currentState )
      {
        case WATCHING:
          msg = "WATCHING"; break;
//         case ALERT:
//           msg = "ALERT"; break;
        case ATTACK:
          msg = "ATTACK"; break;
        case CHASING:
          msg = "CHASING"; break;
      }
#ifdef _DEBUG
    std::cout << "The new state is '" << msg << "'" << std::endl;
#endif
  }

void Enemy::reorient_enemy_to_hero()
  {
    Ogre::Vector3 p = _refHero->getMainNode()->getPosition();

    Ogre::Vector3 o = _rigidBody->getSceneNode()->getPosition();

    Ogre::Vector3 v = p - o; // 1er vector

    Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z; // 2do vector

    Ogre::Radian angle = orientacion.angleBetween ( v );

    if ( orientacion.getRotationTo(v).getYaw().valueDegrees() > 0 )
      {
        if ( angle.valueDegrees() > 5 )
          {
            turn_left();
          }
      }
    else
      {
        if ( angle.valueDegrees() > 5 )
          {
            turn_right();
          }
      }
  }
/*
bool Enemy::isEqualPoint (Ogre::Vector3 point1, Ogre::Vector3 point2) {
  bool igual = false;
  Ogre::Real variacion = 0.1;

  Ogre::Vector3 p = point1 - point2;
  p.x = Ogre::Math::Abs(p.x);
  p.y = Ogre::Math::Abs(p.y);
  p.z = Ogre::Math::Abs(p.z);
  igual = (p.x < variacion) && (p.y < variacion) && (p.z < variacion);

  return igual;
}*/

bool
Enemy::isInContact(){
  bool isCollition = false;
  btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
  int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();
  btPersistentManifold* contactManifold = nullptr;
  btCollisionObject* obA;
  btCollisionObject* obB;
  
  // Recorremos las colisiones que se esten produciendo
  for (int i=0;i<numManifolds && !isCollition;i++) {
    contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
    obA = (btCollisionObject*)(contactManifold->getBody0());
    obB = (btCollisionObject*)(contactManifold->getBody1());
    
    OgreBulletCollisions::Object* obOB_A = _world->findObject(obA);
    OgreBulletCollisions::Object* obOB_B = _world->findObject(obB);
    
    if (obOB_A == _refHero->getRigidBody() || obOB_B == _refHero->getRigidBody()) {
	isCollition = (obOB_A == _rigidBody || obOB_B == _rigidBody);
	if (isCollition){
	 return true;
	}
     }
   }
   return false;
}

void Enemy::walk(bool reverse, float velocidad, float timeSinceLastFrame){
  if (/*!_isShooting && */ _stateObject == LIVE) {
		if (reverse) velocidad *= -1;

//    NOTA: Al estar dentro del update, hace que se repita muchas veces el sonido y peta
//    if ( _type == HERO )
//      _soundWalkFX->play();

		_rigidBody->enableActiveState();
		Ogre::Vector3 currentVelocity = _rigidBody->getLinearVelocity();
		Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
		orientacion = orientacion*velocidad*timeSinceLastFrame;
		orientacion.y = currentVelocity.y; // Movemos solo en los ejes Z y X
		_rigidBody->setLinearVelocity(orientacion );
		if(!_animated){
		  _mAnimation->blend("Walk", AnimationBlender::Switch, .5, true); 
		      std::cout << "walk"<< std::endl;

		_animated  = true;
		}
		changeAnimation(WALK_ANIMATION);
	}
}
 
void Enemy::run(bool reverse, float velocidad, float timeSinceLastFrame){
  if (/*!_isShooting && */ _stateObject == LIVE) {
		if (reverse) velocidad *= -1;

//    NOTA: Al estar dentro del update, hace que se repita muchas veces el sonido y peta
//    if ( _type == HERO )
//      _soundWalkFX->play();

		_rigidBody->enableActiveState();
		Ogre::Vector3 currentVelocity = _rigidBody->getLinearVelocity();
		Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
		orientacion = orientacion*velocidad*timeSinceLastFrame;
		orientacion.y = currentVelocity.y; // Movemos solo en los ejes Z y X
		_rigidBody->setLinearVelocity(orientacion );
// 		if(!_animated){
// 		  _mAnimation->blend("Run", AnimationBlender::Switch, 0.2, true); 
// 		      std::cout << "run"<< std::endl;
// 
// 		_animated  = true;
// 		}
		changeAnimation(RUN_ANIMATION);
	}
} 
void Enemy::death() {
	_particleDeathNode->setVisible(true);
	_stateObject = DEAD;
	setVisible (false);
	_particleDeath->setVisible(true);
}

void Enemy::attack(float timeSinceLastFrame){
  _timeLastAttack += timeSinceLastFrame;
     
  if(_timeLastAttack >= (_mEntity)->getAnimationState(RUN_ANIMATION)->getLength()){
    //animacion de ataque
    _refHero->setHealth(_refHero->getHealth()-ENEMY_DAMAGE);
    _timeLastAttack=0;
//     _mAnimation->blend("Attack", AnimationBlender::Switch, 0.8, false); 
    _animated=false;
    		changeAnimation(ATTACK_ANIMATION);

    std::cout << "timing"<< std::endl;
  }
}


void Enemy::changeAnimation ( const std::string& nameAnimation )
  {
    if ( ( _currentAnimation != nullptr ) &&
        ( _currentAnimation->getAnimationName() != nameAnimation ) )
      {
        Ogre::AnimationState *animation;

        if ( STOP_ANIMATION == nameAnimation )
          {
	     animation = (_mEntity)->getAnimationState(WALK_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(ATTACK_ANIMATION);
            animation->setEnabled(false);
	       animation = (_mEntity)->getAnimationState(RUN_ANIMATION);
            animation->setEnabled(false);
          }
        else if ( RUN_ANIMATION == nameAnimation )
          {
            animation = (_mEntity)->getAnimationState(STOP_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(WALK_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(ATTACK_ANIMATION);
            animation->setEnabled(false);
          }
           else if ( WALK_ANIMATION == nameAnimation )
          {
                animation = (_mEntity)->getAnimationState(STOP_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(RUN_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(ATTACK_ANIMATION);
            animation->setEnabled(false);
          }
         else if ( ATTACK_ANIMATION == nameAnimation )
          {
          animation = (_mEntity)->getAnimationState(STOP_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(WALK_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(RUN_ANIMATION);
            animation->setEnabled(false);
          }
 
        _currentAnimation = (_mEntity)->getAnimationState(nameAnimation);
           _currentAnimation->setEnabled(true);
	   _currentAnimation->setLoop(true);
//        (ATTACK_ANIMATION == nameAnimation)? _currentAnimation->setLoop(false):_currentAnimation->setLoop(true);
	 _currentAnimation->setWeight(1);
    _currentAnimation->setTimePosition(0);
      }
  }
  
  
SceneNode*
Enemy::getLifeNode() const{
  return _lifeNode;
}
Ogre::Billboard*
Enemy::getBillBoardNode() const{
  return _lifeBar;
}

Ogre::BillboardSet*
Enemy::getBillBoardSet() const{
  return _bbSetLife;
}

Ogre::Billboard *
Enemy::getLifeBar() const{
  return _lifeBar;
}

         
void Enemy::updateLifeBar()
  {
//     if ( _health != _lastHealth )
//       {
//         if ( _lastHealth != 0 )
//           {
// //             setCurrentState( SHOOTING );
//             forcedIN = true;
// //             _timeElapsed_Shooting = _timeElapsed_Global;
//           }

//         _lastHealth = _health;

        Ogre::Real ratio = _life / MAX_HEALTH;

        if (ratio < 0.0f)
          ratio = 0.0f;

        _lifeBar->setTexcoordRect((1.0 - ratio) / SIZE_LIFE_BAR,
                                  0.0f,
                                  0.50f + (1.0 - ratio) / SIZE_LIFE_BAR,
                                  1.0);
      
  }