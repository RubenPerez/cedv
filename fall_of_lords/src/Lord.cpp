#include "Lord.h"


Lord::Lord( Ogre::SceneManager* sceneMgr,
                    OgreBulletDynamics::DynamicsWorld* world,
                    const std::string& name, const std::string& mesh,
                    const Ogre::Vector3& v_pos,
                    Character* ptrHero ) : Enemy ( sceneMgr,
                                                        world,
                                                        name, mesh,
                                                        v_pos,ptrHero)
  {
    _soundAttack = SoundFXManager::getSingleton().load("hit.wav");

  }
  
  
  Lord::~Lord(){
//     _soundAttack=nullptr;
  }
  
  
  
void
Lord::createLifeBar(){
   _bbSetLife = _mSceneMgr->createBillboardSet();
    _bbSetLife->setMaterialName("lifeBar");
    _lifeBar = _bbSetLife->createBillboard(Ogre::Vector3(4, 4, -4));
    _lifeBar->setDimensions(1, 0.1);
    _lifeBar->setTexcoordRect(0.0f, 0.0f, 0.5f, 1.0f);
    
    _lifeNode = _mMainNode->createChildSceneNode();

    _lifeNode->attachObject(_bbSetLife);
        _lifeNode->setVisible(true);

 }
 

 
void /*Enemy::update ( double timeSinceLastFrame, std::vector<Character*>   vCharacteres)*/
Lord::update(float timeSinceLastFrame, OIS::Keyboard const*, OIS::Mouse const*)
  {
    
if (_stateObject != END) {
      if (_stateObject == DEAD) {
			_timerParticleDeath += timeSinceLastFrame;
			_particleDeath->setEmitting(true);
			if (_timerParticleDeath > TIMER_PATICLE_DEATH) {
				_particleDeath->setEmitting(false);
				_stateObject = END;
			}
		} else {
			if ( _currentAnimation != nullptr )
			{
				_currentAnimation->addTime(timeSinceLastFrame );
			}

// 			if (_nodeShot && _isShooting) {
// 				updateShot(timeSinceLastFrame, vCharacteres);
// 			}

			_timerParticleDeath = 0.0;
    }
}
  
     if ( _stateObject == LIVE )
      {
//         _timeElapsed_Global += timeSinceLastFrame;

        updateLifeBar();

        // Cambios de estado del enemigo

        switch ( _currentState )
          {
            // ################## ESTADO WATCHING ##################
            // Si estamos en el estado normal de WATCHING
            case WATCHING:
                // Si avistamos al heroe :
                // - tomamos su posiciÃ³n actual
                // - cogemos el tiempo actual
                // - pasamos al estado ALERT CHASING
	         changeAnimation(STOP_ANIMATION);

                if ( haveYouSeenAnybody() )
                  {
//                     _positionLastViewed = _refHero->getPosition();
//                     _timeFirstVision = _timeElapsed_Global;
                    setCurrentState ( CHASING );
                  }
                // Si no vemos al heroe :
                // - reseteamos a posicion inicial
                
                else
                  {
// 		    walk_to(_route.getNextPoint(_mMainNode->getPosition()), false);//////////////////
                      ;
                  }

                break;

	    case ATTACK:
	      if ( isInContact() ){
		   reorient_enemy_to_hero();
		   attack(timeSinceLastFrame);	  
	      }
	       setCurrentState(CHASING);
	      break;
	      
	    case CHASING:
	      
	      if(haveYouSeenAnybody() ) {
		reorient_enemy_to_hero();
		if(isInContact()){
		  setCurrentState(ATTACK);
		}
		else{
		  walk_to(_refHero->getMainNode()->getPosition(), timeSinceLastFrame,false);;
		  ;
		}
	      }
	      else
		 {
		   _timeLostVision +=timeSinceLastFrame;
		   if(_timeLostVision >= 5){
		     setCurrentState(WATCHING); _timeLostVision=0;}
		     
		   else{
		     walk_to(_route.getCenter() ,timeSinceLastFrame,false);
		   }
// 		   if(_mMainNode->getPosition() != _route.getCenter())
// 		    walk_to(_route.getCenter() ,timeSinceLastFrame,false);
// 		   else
// 		     setCurrentState(WATCHING);
                  }
		
	    break;
	      
    }
  }
  }
  
  
void Lord::death() {
	_particleDeathNode->setVisible(true);
	_stateObject = DEAD;
	setVisible (false);
	_particleDeath->setVisible(true);
}



void Lord::attack(float timeSinceLastFrame){
  _timeLastAttack += timeSinceLastFrame;
     
  if(_timeLastAttack >= (_mEntity)->getAnimationState(WALK_ANIMATION)->getLength()){
    //animacion de ataque
    _refHero->setHealth(_refHero->getHealth()-ENEMY_DAMAGE);
    _timeLastAttack=0;
//     _mAnimation->blend("Attack", AnimationBlender::Switch, 0.8, false); 
    _animated=false;
    		changeAnimation(ATTACK_ANIMATION);
_soundAttack->play();
    std::cout << "timing"<< std::endl;
  }
}


void Lord::changeAnimation ( const std::string& nameAnimation )
  {
    if ( ( _currentAnimation != nullptr ) &&
        ( _currentAnimation->getAnimationName() != nameAnimation ) )
      {
        Ogre::AnimationState *animation;

        if ( STOP_ANIMATION == nameAnimation )
          {
	     animation = (_mEntity)->getAnimationState(WALK_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(ATTACK_ANIMATION);
            animation->setEnabled(false);
	   
          }
       
           else if ( WALK_ANIMATION == nameAnimation )
          {
                animation = (_mEntity)->getAnimationState(STOP_ANIMATION);
            animation->setEnabled(false);
	   
	     animation = (_mEntity)->getAnimationState(ATTACK_ANIMATION);
            animation->setEnabled(false);
          }
         else if ( ATTACK_ANIMATION == nameAnimation )
          {
          animation = (_mEntity)->getAnimationState(STOP_ANIMATION);
            animation->setEnabled(false);
	     animation = (_mEntity)->getAnimationState(WALK_ANIMATION);
            animation->setEnabled(false);
	   
          }
 
        _currentAnimation = (_mEntity)->getAnimationState(nameAnimation);
           _currentAnimation->setEnabled(true);
	   _currentAnimation->setLoop(true);
//        (ATTACK_ANIMATION == nameAnimation)? _currentAnimation->setLoop(false):_currentAnimation->setLoop(true);
	 _currentAnimation->setWeight(1);
    _currentAnimation->setTimePosition(0);
      }
  }
  