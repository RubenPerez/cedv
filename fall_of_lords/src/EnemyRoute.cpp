#include "EnemyRoute.h"
#include <iostream>

using namespace std;

EnemyRoute::EnemyRoute(Ogre::Vector3 center, float radius)
{
//   clear();
  _center = center;
  _radius = radius;
  _currentPoint = center;
}

EnemyRoute::EnemyRoute(){
}

EnemyRoute::~EnemyRoute()
{
}


/*
EnemyRoute::EnemyRoute(const EnemyRoute& other)
{
  copy ( other );
}

EnemyRoute& EnemyRoute::operator=(const EnemyRoute& rhs)
{
  if (this == &rhs) return *this; // handle self assignment

  copy ( rhs );

  return *this;
}

void EnemyRoute::copy ( const EnemyRoute& source )
{
  _center = source.getCenter();
  _radius = source.getRadius();
  _currentPoint = source.getCurrentPoint();

}*/


Ogre::Vector3 EnemyRoute::getNextPoint(Ogre::Vector3 posChar) {
  float randX, randZ;
  float posx = posChar.x, posz = posChar.z;
  srand(time(NULL));
  randX = rand_FloatRange(_radius- posx);
  randZ = rand_FloatRange(_radius- posz);
  
        if(randX >= posChar.x  )
    {
       _currentPoint.x= posChar.x +0.1;
    }
    else if (randX < posChar.x )
    {
      _currentPoint.x= posChar.x - 0.1;
    }

    if(randZ >= posChar.z  )
    {
       _currentPoint.z= posChar.z +0.1;
    }
    else if (randZ < posChar.z )
    {
      _currentPoint.z= posChar.z - 0.1;
    }
    _currentPoint.y = posChar.y;
    
    return _currentPoint;
    
}

Ogre::Vector3 EnemyRoute::getCenter() const{
  return _center;
}

float EnemyRoute::getRadius() const{
  return _radius;
}

Ogre::Vector3 EnemyRoute::getCurrentPoint() const {
  return _currentPoint;
}

float
EnemyRoute::rand_FloatRange(float a){
  float c;
  c = (float)rand()/ a;
  return c;
}
