#include "MiniMapTextureListener.h"


MiniMapTextureListener::MiniMapTextureListener ( Ogre::SceneManager* sceneMgr, std::list<Character*> *vCharacteres)
{
  _vCharacteres = vCharacteres;
  _sceneMgr = sceneMgr;
}

MiniMapTextureListener::~MiniMapTextureListener()
{
//   _vCharacteres.clear();
  _sceneMgr = nullptr;
}

void MiniMapTextureListener::preRenderTargetUpdate ( const RenderTargetEvent& evt )
  {
	// Desactivamos las sombras en el Minimapa
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_NONE);

	// Recorremos todos los personajes y los mostramos como cubos en el mapa
	for (Character* character: *_vCharacteres) {
		if (character->isVisible()) {
			character->showDummy(true);
			character->getMainEntity()->setCastShadows(false);
		}
	}
  }

void MiniMapTextureListener::postRenderTargetUpdate ( const RenderTargetEvent& evt )
  {
	// Activamos las sombras
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	// Recorremos todos los personajes y los mostramos con su entidad original
    	for (Character* character: *_vCharacteres) {
		if (character->isVisible()) {
			character->showDummy(false);
			character->getMainEntity()->setCastShadows(true);
		}
	}
  }

