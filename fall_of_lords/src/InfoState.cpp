#include "InfoState.h"


template<> InfoState* Ogre::Singleton<InfoState>::msSingleton = 0;

void
InfoState::enter ()
{  
  GameManager::getSingletonPtr()->getLog()->logMessage("Entering credit state");
  Ogre::Root* root = Ogre::Root::getSingletonPtr();

    _sceneMgr = root->createSceneManager(Ogre::ST_GENERIC, "InfoSceneManager");
  _camera = _sceneMgr->createCamera("InfoCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
  
   Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);
  
  createBackground();
  createGUI();

}



void InfoState::createBackground(){
  
  
  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Fondo", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("fondo.jpg");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
  
  // Create background rectangle covering the whole screen
  _rect = new Ogre::Rectangle2D(true);
  _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  _rect->setMaterial("Fondo");
  
  //   Render the background before everything else
  _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
  

  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Fondo");
  node->attachObject(_rect);
  
//   material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.02, 0.0);
  
}

void InfoState::createGUI()
{
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Ex1/Sheete");
  
  _about = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/About");
  _about->setText("Volver");
  _about->setSize(CEGUI::UVector2(CEGUI::UDim(0.3,0),CEGUI::UDim(0.08,0)));
  _about->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05,0),CEGUI::UDim(0.80,0)));
  _about->subscribeEvent(CEGUI::PushButton::EventClicked,
 			     CEGUI::Event::Subscriber(&InfoState::back, 
						      this));
  _sheet->addChildWindow(_about);
  
  _controls = CEGUI::WindowManager::getSingleton().createWindow("OgreTray/Button","Ex1/Controls");
//   _text = "Controls";
  _controls->setSize(CEGUI::UVector2(CEGUI::UDim(0.3,0),CEGUI::UDim(0.08,0)));
  _controls->setPosition(CEGUI::UVector2(CEGUI::UDim(0.65,0),CEGUI::UDim(0.80,0)));
  _controls->subscribeEvent(CEGUI::PushButton::EventClicked,
 			     CEGUI::Event::Subscriber(&InfoState::start1, 
						      this));
  _sheet->addChildWindow(_controls);
  
  CEGUI::System::getSingleton().setGUISheet(_sheet);
    Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
    Ogre::Overlay *overlay = overlayManager->getByName("Sinopsis");
    overlay->show();
//     _text = "Sinopsis";
    _controls->setText("Controles");
}

bool InfoState::start1 (const CEGUI::EventArgs &e){
  
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  if (_controls->getText() == "Sinopsis"){
    
    Ogre::Overlay *overlay = overlayManager->getByName("Controls");
    overlay->hide();
    
    overlay = overlayManager->getByName("Sinopsis");
    overlay->show();
//     _text = "Sinopsis";
    _controls->setText("Controles");

    }
  else{
     Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
      Ogre::Overlay *overlay = overlayManager->getByName("Sinopsis");
      overlay->hide();
      overlay = overlayManager->getByName("Controls");
      overlay->show();
       _controls->setText("Sinopsis");

  }
  return true;
}

bool InfoState::back (const CEGUI::EventArgs &e){
  this->popState();
  return true;
}

void
InfoState::exit ()
{ 
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Controls");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheete");
 
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheete");
  
  Ogre::MaterialManager::getSingleton().remove("Fondo");
   _sceneMgr->destroyCamera(_camera);

  delete _rect;
  
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  Ogre::Overlay *overlay = overlayManager->getByName("Sinopsis");
  overlay->hide();
  overlay = overlayManager->getByName("Controls");
  overlay->hide();

  _sceneMgr->destroySceneNode("Fondo");
  if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);


}

void
InfoState::pause()
{
//   CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/About");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Controls");
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheete");
 
  CEGUI::WindowManager::getSingleton().destroyWindow("Ex1/Sheete");
  
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  Ogre::Overlay *overlay = overlayManager->getByName("Sinopsis");
  overlay->hide();
  overlay = overlayManager->getByName("Controls");
  overlay->hide();
}

void
InfoState::resume()
{
}

bool
InfoState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
InfoState::frameEnded
(const Ogre::FrameEvent& evt)
{
  return _exitGame;
}

void
InfoState::keyPressed
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_P) {
    popState();
  } 
  
}

void
InfoState::keyReleased
(const OIS::KeyEvent &e)
{
  
}

void
InfoState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
InfoState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  
}

void
InfoState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

InfoState*
InfoState::getSingletonPtr ()
{
return msSingleton;
}

InfoState&
InfoState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}


 
