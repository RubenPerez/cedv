
 
#include "Item.h"

Item::Item( Ogre::SceneManager* sceneMgr, 
                    OgreBulletDynamics::DynamicsWorld* world,
                    const std::string& name, const std::string& mesh,
                    const Ogre::Vector3& v_pos,
                    Character* ptrHero, const std::string& music ) : InteractiveObject ( sceneMgr,
                                                        world,
                                                        name, mesh,
                                                        v_pos )
  {
	_timerParticleLiberate = 0.0;
//crear nodo y praticulas
	 _particleLiberation = _mSceneMgr->createParticleSystem("particleEnemy" + _mName, "particleEnemy");
// 	_particleLiberation->setVisible(false);
	// Creamos un nodo
	_particleLiberationNode = _mMainNode->createChildSceneNode("particleLiberation" + _mName);
	// Ajuntamos las partículas al nodo
	_particleLiberationNode->attachObject(_particleLiberation);
    _particleLiberationNode->setVisible(false);
	
//     _entityDummy->setMaterialName ( "MaterialAmarillo" );
    _refHero = ptrHero;

	_soundFreeFX = SoundFXManager::getSingleton().load(music);
  }

Item::~Item()
{
	// Destruimos el nodo
	_mSceneMgr->destroySceneNode(_particleLiberationNode);
// 	// Destruimos el sistema de partículas
	_mSceneMgr->destroyParticleSystem(_particleLiberation);
	_refHero=nullptr;
}



void Item::update ( Real timeSinceLastFrame) {

  if (_stateObject != END) {
      if (_stateObject == DEAD) {
			_timerParticleLiberate += timeSinceLastFrame;
			_particleLiberation->setEmitting(true);
			if (_timerParticleLiberate > TIMER_PATICLE_LIBERATE) {
				_particleLiberation->setEmitting(false);
				_stateObject = END;
			}
		} else {
			if ( _currentAnimation != nullptr )
			{
				_currentAnimation->addTime(timeSinceLastFrame );
			}
			_timerParticleLiberate = 0.0;
    }
}
}

