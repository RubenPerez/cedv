
#include "OgreCharacter.h"


OgreCharacter::OgreCharacter (Ogre::String name, Ogre::String mesh, SceneManager *sceneMgr, Ogre::Vector3 posIni,  
			    OgreBulletDynamics::DynamicsWorld* world, int life, int pm
):Character( sceneMgr, world, name, mesh, posIni) {
  _pm = pm;
  _visible = true;
  _inHands  = false;
  _state = States::IDLE; 
  // Setup basic node structure to handle 3rd person cameras
  _magicNode=nullptr;
  //Aux nodes for the camera
  _mSightNode = _mMainNode->createChildSceneNode (_mName + "_sight", Vector3 (0, 3, 20));
  _mCameraNode = _mMainNode->createChildSceneNode (_mName + "_camera", Vector3 (0, 1.5, -2.5));
 
//   this->createLifeBar();
//   this->createMagicBar();
  _animated = false;
  _key=false;
}

void 
OgreCharacter::showDummy(bool show){
      this->getMainEntity()->setVisible(!show);
      this->getDummyEntity()->setVisible(show);
}
         
OgreCharacter::~OgreCharacter () {
  
    
    if(_mSightNode) _mSceneMgr->destroySceneNode(_mSightNode);
    if(_mCameraNode) _mSceneMgr->destroySceneNode(_mCameraNode);
    // Destruimos el billboardset
//     if(_magicNode && _bbSetLife)_magicNode->detachObject(_bbSetLife);
//     if(_magicNode)_mSceneMgr->destroySceneNode(_magicNode);

}
 
 void OgreCharacter::update (Ogre::Real elapsedTime, const  OIS::Keyboard* input, const OIS::Mouse* mouse) {
   // Handle movement
//    _rigidBody->enableActiveState();
   std::string type;
   int speed;
//    Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
//    switch(_state){
//      case IDLE:
//        break;
//      case RUNNING:
//        speed = 8;
//       orientacion.y = 0.0; // Movemos solo en los ejes Z y X
//       _rigidBody->setLinearVelocity(orientacion * speed);
//        break;
//      case JUMP:
//        break;
//      case ATTACK1:
//        break;
//      case WALKING:
//        speed = 2;
//       orientacion.y = 0.0; // Movemos solo en los ejes Z y X
//       _rigidBody->setLinearVelocity(orientacion * speed);
//        break;
//   }
  
   if(input->isKeyDown(OIS::KC_Q)){
     type = "Run";
     speed = 8;
     // 	      animated =false;
   }
   else{
     type = "Walk";
     speed = 2;
   }  
   if(_inHands) type = type+"_Sword";
   if(input->isKeyDown (OIS::KC_R)){
      _state=States::WALKING;
     if(!_animated){ 
       if(_inHands){
	 _mAnimation->blend("Sheathe", AnimationBlender::Switch, 0, false);
	 SoundFXPtr sonido = SoundFXManager::getSingletonPtr()->load("sword_unsheat.wav");
	 sonido->play();
	 _inHands=false;
      }
      else if(!_inHands){
	_mAnimation->blend("Unsheathe", AnimationBlender::Switch, 0, false);
	SoundFXPtr sonido = SoundFXManager::getSingletonPtr()->load("sword_sheath.wav");
	 sonido->play();
	_inHands=true;
      }
      _animated  = true;
     }
   }
  
 
   if (input->isKeyDown (OIS::KC_W)) {
           _state=States::WALKING;

     if(!_animated){ _mAnimation->blend(type, AnimationBlender::Switch, 0, true); _animated  = true;}
//      _mMainNode->translate (_mMainNode->getOrientation () * Vector3 (0, 0, speed * elapsedTime));
		Ogre::Vector3 currentVelocity = _rigidBody->getLinearVelocity();
     	Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
	orientacion = orientacion*speed;
	orientacion.y = currentVelocity.y; // Movemos solo en los ejes Z y X
	_rigidBody->setLinearVelocity(orientacion);

 }
   if (input->isKeyDown (OIS::KC_S) ) {
           _state=States::WALKING;

     if(!_animated) {_mAnimation->blend(type, AnimationBlender::Switch, 0.2, true); _animated = true;}
//      _mMainNode->translate (_mMainNode->getOrientation () * Vector3 (0, 0, -5 * elapsedTime));
    		Ogre::Vector3 currentVelocity = _rigidBody->getLinearVelocity();

	Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
	orientacion = orientacion*speed;
	orientacion.y = currentVelocity.y; // Movemos solo en los ejes Z y X
	_rigidBody->setLinearVelocity(orientacion);
     
   }
   if (input->isKeyDown (OIS::KC_A)) {
           _state=States::WALKING;

//      _mMainNode->yaw (Radian (2 * elapsedTime));
     this->turn_left();
//           _rigidBody->getBulletRigidBody()->yaw (Radian (2 * elapsedTime));

     
   }
   if (input->isKeyDown (OIS::KC_D)) {
           _state=States::WALKING;

//      _mMainNode->yaw (Radian (-2 * elapsedTime));
          this->turn_right();
	  
   }
   _mAnimation->addTime(elapsedTime);
   
 }
 
// void keyPressed(){
//   if(input->isKeyDown (OIS::KC_E)) {
//       if(_inHands && !_animated){
//     	 _mAnimation->blend("Attack", AnimationBlender::Switch, 0, false);
// 	 SoundFXPtr sonido = SoundFXManager::getSingletonPtr()->load("hit.wav");
// 	 sonido->play();
// 	 _animated=true;
// 
//       }
//   }
//   
//   if (input->isKeyDown (OIS::KC_SPACE)) {
//      if(!_animated){ _mAnimation->blend("Jump", AnimationBlender::Switch, 0, false); _animated  = true;}
// //      _mMainNode->translate (_mMainNode->getOrientation () * Vector3 (0, 0, speed * elapsedTime));
//      	Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
// 		orientacion.y = 1.0; // Movemos solo en los ejes Z y X
// 		_rigidBody->applyImpulse(orientacion * speed, _rigidBody->getCenterOfMassPosition());
// _rigidBody->setLinearVelocity(orientacion * speed);
//  }
// }

bool OgreCharacter::onGround(){
  return _rigidBody->getLinearVelocity().y == 0;
}

void OgreCharacter::runStart(){
  _state = States::RUNNING;

  if(_inHands)_mAnimation->blend("Run_Sword", AnimationBlender::Switch, 1, true);
  else   _mAnimation->blend("Run", AnimationBlender::Switch, 1, true);
}
void OgreCharacter::runStop(){
 
  _state = States::IDLE;

  if(_inHands)_mAnimation->blend("Idle_Sword", AnimationBlender::Switch, 1, true);
  else   _mAnimation->blend("Idle", AnimationBlender::Switch, 1, true);
}
void OgreCharacter::walkStart(){
   _state = States::WALKING;
  if(_inHands)_mAnimation->blend("Walk_Sword", AnimationBlender::Switch, 1, true);
  else   _mAnimation->blend("Walk", AnimationBlender::Switch, 1, true);
}
void OgreCharacter::walkStop(){
  _state = States::IDLE;
  if(_inHands)_mAnimation->blend("Idle_Sword", AnimationBlender::Switch, 1, true);
  else   _mAnimation->blend("Idle", AnimationBlender::Switch, 1, true);
}
void OgreCharacter::jumpStart(){
   _state = States::JUMPING;
   int speed = _state==States::RUNNING?8:2;
/*if(!_animated){*/ _mAnimation->blend("Jump", AnimationBlender::Switch, 0, false);/* _animated  = true;}*/
//      _mMainNode->translate (_mMainNode->getOrientation () * Vector3 (0, 0, speed * elapsedTime));
     	Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
		orientacion.y = 1.0; // Movemos solo en los ejes Z y X
		_rigidBody->applyImpulse(orientacion * speed, _rigidBody->getCenterOfMassPosition());
_rigidBody->setLinearVelocity(orientacion * speed);
//   if(_inHands)_mAnimation->blend("Jump", AnimationBlender::Switch, 1, true);
//   else   _mAnimation->blend("Jump_Sword", AnimationBlender::Switch, 1, true);
}
void OgreCharacter::jumpStop(){
  _state = States::IDLE;
  if(_inHands)_mAnimation->blend("Idle_Sword", AnimationBlender::Switch, 1, true);
  else   _mAnimation->blend("Idle", AnimationBlender::Switch, 1, true);
}
void OgreCharacter::atack1Start(){
  if(_inHands /*&& !_animated*/){
	_state = States::ATTACK1;
    	 _mAnimation->blend("Attack", AnimationBlender::Switch, 0, false);
	 SoundFXPtr sonido = SoundFXManager::getSingletonPtr()->load("hit.wav");
	 _attack1->play();
// 	 _animated=true;

      }
}
void OgreCharacter::atack1Stop(){
}
void OgreCharacter::atack2Start(){
}
void OgreCharacter::atack2Stop(){
}

bool OgreCharacter::isAttack(){
  return _state == States::ATTACK1;
}

/*
 void 
 OgreCharacter::createLifeBar(){
   _bbSetLife = _mSceneMgr->createBillboardSet();
    _bbSetLife->setMaterialName("lifeBar");
    _lifeBar = _bbSetLife->createBillboard(Ogre::Vector3(0, 1.6, 0));
    _lifeBar->setDimensions(1, 0.1);
    _lifeBar->setTexcoordRect(0.0f, 0.0f, 0.5f, 1.0f);
    
    _lifeNode = _mMainNode->createChildSceneNode();

    _lifeNode->attachObject(_bbSetLife);
 }*/
//   void 
//  OgreCharacter::createMagicBar(){
//    _bbSetLife = _mSceneMgr->createBillboardSet();
//     _bbSetLife->setMaterialName("lifeBar");
//     _magicBar = _bbSetLife->createBillboard(Ogre::Vector3(0, 2, 0));
//     _magicBar->setDimensions(1, 0.1);
//     _magicBar->setTexcoordRect(0.0f, 0.0f, 0.5f, 1.0f);
//     
//     _magicNode = _mMainNode->createChildSceneNode();
// 
//     _magicNode->attachObject(_bbSetLife);
//  }
//  
 void 
 OgreCharacter::swapRun2Walk(){
   _animated = false;
   if(_state==States::WALKING) _state=States::RUNNING;
   else _state=States::WALKING;
 }
 
 void 
 OgreCharacter::turnFrontBack(){
   _mMainNode->yaw(Degree(180)); 
 }
 
 void 
 OgreCharacter::stopAnimation(){
   if(!_inHands) 
     _mAnimation->blend("Idle", AnimationBlender::Switch, 0.2, true);
   else if(_inHands)
     _mAnimation->blend("Idle_Sword", AnimationBlender::Switch, 0.2, true);
  _rigidBody->setLinearVelocity(Ogre::Vector3::ZERO);

   _animated=false;
 }
 
 void OgreCharacter::death(){
 }
 
 void 
 OgreCharacter::setVisible (bool visible) {
   _visible = visible;
   _mMainNode->setVisible (visible);
   if (visible) {
      _world->getBulletDynamicsWorld()->addCollisionObject(_rigidBody->getBulletObject());
    } else {
      _world->getBulletDynamicsWorld()->removeCollisionObject(_rigidBody->getBulletObject());
  }
     
 }
 