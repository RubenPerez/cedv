#include "CreditState.h"


template<> CreditState* Ogre::Singleton<CreditState>::msSingleton = 0;

void
CreditState::enter ()
{  
  GameManager::getSingletonPtr()->getLog()->logMessage("Entering credit state");
  Ogre::Root* root = Ogre::Root::getSingletonPtr();

    _sceneMgr = root->createSceneManager(Ogre::ST_GENERIC, "CreditSceneManager");
  _camera = _sceneMgr->createCamera("CreditCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
  
   Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);
  
  
  createBackground();
  

}



void CreditState::createBackground(){
  
  
  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Backgr", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("creditos_2.jpg");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
  
  // Create background rectangle covering the whole screen
  _rect = new Ogre::Rectangle2D(true);
  _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  _rect->setMaterial("Backgr");
  
  //   Render the background before everything else
  _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
  

  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Backgr");
  node->attachObject(_rect);
  
//   material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.02, 0.0);
  
}


void
CreditState::exit ()
{ 
  Ogre::MaterialManager::getSingleton().remove("Backgr");
   _sceneMgr->destroyCamera(_camera);

  delete _rect;

  _sceneMgr->destroySceneNode("Backgr");
  if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);


}

void
CreditState::pause()
{
}

void
CreditState::resume()
{
}

bool
CreditState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
CreditState::frameEnded
(const Ogre::FrameEvent& evt)
{
  return _exitGame;
}

void
CreditState::keyPressed
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_P) {
    popState();
  } 
  
}

void
CreditState::keyReleased
(const OIS::KeyEvent &e)
{
  
}

void
CreditState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
CreditState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  
}

void
CreditState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

CreditState*
CreditState::getSingletonPtr ()
{
return msSingleton;
}

CreditState&
CreditState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}


