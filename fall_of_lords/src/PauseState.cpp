#include "PauseState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;


void
PauseState::enter ()
{    std::cout << "Pause state enter"<<std::endl;

   Ogre::Root* root = Ogre::Root::getSingletonPtr();
  _sceneMgr = root->createSceneManager(Ogre::ST_EXTERIOR_REAL_FAR,"PauseManager");
  _camera = _sceneMgr->createCamera("PauseCamera");
  _camera->setPosition(Ogre::Vector3(5,10.5,20));
  _camera->lookAt(Ogre::Vector3(1.4,4.3,3));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _camera->setFOVy(Ogre::Degree(48));
  
  Ogre::Viewport * viewport = GameManager::getSingletonPtr()->getViewport();
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  viewport->setCamera(_camera);
  
  _exitGame=false;
//   _overlay = Ogre::OverlayManager::getSingletonPtr()->getByName("Pause");
//   _overlay->show();
  
  CEGUI::MouseCursor::getSingleton().show();

  
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Backr", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("waterwall.jpg");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
  
  // Create background rectangle covering the whole screen
  _rect = new Ogre::Rectangle2D(true);
  _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  _rect->setMaterial("Backr");
  
  //   Render the background before everything else
  _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
  

  _node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Backr");
  _node->attachObject(_rect);
  
  material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.02, 0.0);
  
   //Sheet
  CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Sheet");

  //Config Window
  CEGUI::Window* formatWin = CEGUI::WindowManager::getSingleton().loadWindowLayout("pauseMenu.layout");
  
  //Exit Window
   CEGUI::Window* resumeButton = CEGUI::WindowManager::getSingleton().getWindow("Pause/ResumeButton");
  resumeButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&PauseState::resume, 
						      this));
  
   CEGUI::Window* controlsButton = CEGUI::WindowManager::getSingleton().getWindow("Pause/ControlsButton");
  controlsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&PauseState::controls, 
						      this));
  
  CEGUI::Window* exitButton = CEGUI::WindowManager::getSingleton().getWindow("Pause/ExitButton");
  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&PauseState::quit, 
						      this));
  
 
  
  //Attaching buttons
  sheet->addChildWindow(formatWin);
  CEGUI::System::getSingleton().setGUISheet(sheet);
}


bool PauseState::resume (const CEGUI::EventArgs &e){
  Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  Ogre::Overlay *overlay = overlayManager->getByName("Controls_easy");
    overlay->hide();
  popState();  
  return true;
}

bool PauseState::controls (const CEGUI::EventArgs &e){
//   popState();  
//    _overlay = Ogre::OverlayManager::getSingletonPtr()->getByName("Pause");
//    _overlay->show();
     Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
  if (CEGUI::WindowManager::getSingleton().getWindow("Pause/ControlsButton")->getText() == "Controles"){
    
    Ogre::Overlay *overlay = overlayManager->getByName("Controls_easy");
    overlay->show();
//     _text = "Sinopsis";
    CEGUI::WindowManager::getSingleton().getWindow("Pause/ControlsButton")->setText("Ocultar");

    }
  else{
     Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
      Ogre::Overlay *overlay = overlayManager->getByName("Controls_easy");
      overlay->hide();
       CEGUI::WindowManager::getSingleton().getWindow("Pause/ControlsButton")->setText("Controles");

  }
  return true;
}

bool PauseState::quit (const CEGUI::EventArgs &e){
//   popState();
  popAllStatesAndPushState(IntroState::getSingletonPtr()); 
  return true;
}

void
PauseState::exit ()
{
//     CEGUI::WindowManager::getSingleton().getWindow("Pause")->hide();
//      CEGUI::WindowManager::getSingleton().getWindow("Pause")->deactivate();
//      CEGUI::WindowManager::getSingleton().getWindow("Pause")->releaseInput();
  _sceneMgr->destroyCamera(_camera);
    CEGUI::WindowManager::getSingleton().destroyWindow("Pause");
// //     CEGUI::WindowManager::getSingleton().destroyWindow("Pause/ExitButton");
// //     CEGUI::WindowManager::getSingleton().destroyWindow("Pause/ExitButton");
    CEGUI::WindowManager::getSingleton().destroyWindow("Sheet");
    delete _rect;
  _sceneMgr->destroySceneNode(_node);
  CEGUI::MouseCursor::getSingleton().hide();
    if(_sceneMgr) GameManager::getSingletonPtr()->getRoot()->destroySceneManager(_sceneMgr);
//   _overlay->hide();
}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P) {
    popState();
  }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

PauseState*
PauseState::getSingletonPtr ()
{
return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
