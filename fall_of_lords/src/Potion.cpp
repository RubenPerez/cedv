 /*
  * ########################################### 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Year : 2015
# Author: Ruben Perez Pascual <ruben11291@gmail.com>
# Computer Engineer at Universidad de Castilla La Mancha
# Author: Angel Peralta Lopez <aperaltalo14@gmail.com>
# Computer Engineer student at Universidad de Castilla La Mancha
*
*/
 
#include "Potion.h"

Potion::Potion( Ogre::SceneManager* sceneMgr,
                    OgreBulletDynamics::DynamicsWorld* world,
                    const std::string& name, const std::string& mesh,
                    const Ogre::Vector3& v_pos,
                    Character* ptrHero ,const std::string& music, int life) : Item ( sceneMgr,
                                                        world,
                                                        name, mesh,
                                                        v_pos,ptrHero, music )
  {
	_recoverLife = life;
	_currentAnimation = (_mEntity)->getAnimationState("Idle");
           _currentAnimation->setEnabled(true);
	   _currentAnimation->setLoop(true);
//        (ATTACK_ANIMATION == nameAnimation)? _currentAnimation->setLoop(false):_currentAnimation->setLoop(true);
	 _currentAnimation->setWeight(1);
    _currentAnimation->setTimePosition(0);
  }

Potion::~Potion()
{
	
}



void Potion::effect(){
  if((_refHero->getHealth() + _recoverLife) >= 100)
  _refHero->setHealth(100);
  else
    _refHero->setHealth(_refHero->getHealth()+_recoverLife);
  _stateObject=DEAD;
  _soundFreeFX->play();
}


